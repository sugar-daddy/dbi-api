package main

import (
	"dbi-api/app"

	_ "github.com/lib/pq"
)

func main() {
	app.StartApplication()
}
