package controller

import (
	JWTService "dbi-api/modules/authentication/service"

	"github.com/gin-gonic/gin"

	ProcurementService "dbi-api/modules/procurement/service"
)

func Routes(router *gin.Engine) {
	procurement := router.Group("/")
	procurement.Use(JWTService.Authenticate([]string{"admin", "purchasing"}))
	{
		procurement.GET("/vendor", ProcurementService.GetVendor)
		procurement.POST("/procurement", ProcurementService.AddPO)
		procurement.POST("/procurement/vendor", ProcurementService.AddVendor)
	}

	procurement2 := router.Group("/")
	procurement2.Use(JWTService.Authenticate([]string{"admin", "finance"}))
	{
		procurement2.POST("/purchase_invoice", ProcurementService.AddPurchaseInvoice)
	}

	procurement3 := router.Group("/")
	procurement3.Use(JWTService.Authenticate([]string{"admin", "purchasing", "finance"}))
	{
		procurement3.GET("/procurement", ProcurementService.GetPO)
	}
}
