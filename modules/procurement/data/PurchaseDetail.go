package data

import inventory "dbi-api/modules/inventory/data"

type PurchaseDetail struct {
	ID            int                `form:"id" json:"id"`
	Material      inventory.Material `form:"material" json:"material"`
	Price         int                `form:"price" json:"price"`
	Quantity      int                `form:"quantity" json:"quantity"`
	PurchaseOrder PurchaseOrder      `form:"purchaseorder" json:"purchaseorder"`
}

type PurchaseDetailOrderResponse struct {
	Status  int              `form:"status" json:"status"`
	Message string           `form:"message" json:"message"`
	Data    []PurchaseDetail `form:"data" json:"data"`
}
