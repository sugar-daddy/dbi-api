package data

import "time"

type PurchaseInvoice struct {
	ID            int           `form:"id" json:"id"`
	Date          time.Time     `form:"date" json:"date"`
	PurchaseOrder PurchaseOrder `form:"purchaseorder" json:"purchaseorder"`
	EmployeeID    string        `form:"employee_id" json:"employee_id"`
}
