package data

import (
	sales "dbi-api/modules/sales/data"
	"time"
)

type PurchaseOrder struct {
	ID              int              `form:"id" json:"id"`
	Date            time.Time        `form:"date" json:"date"`
	Vendor          sales.External   `form:"vendor" json:"vendor"`
	PurchaseDetails []PurchaseDetail `form:"purchasedetails" json:"purchasedetails"`
	EmployeeID      int              `form:"employeeid" json:"employeeid"`
	Invoice_exist   int              `form:"invoice_exist" json:"invoice_exist"`
}

type PurchaseOrderResponse struct {
	Status  int             `form:"status" json:"status"`
	Message string          `form:"message" json:"message"`
	Data    []PurchaseOrder `form:"data" json:"data"`
}
