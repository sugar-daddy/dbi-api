package repository

import (
	"database/sql"
	"dbi-api/database"
	PurchaseOrder "dbi-api/modules/procurement/data"
	ProductionRepository "dbi-api/modules/production/repository"
	External "dbi-api/modules/sales/data"
	"fmt"
	"log"
	"strconv"
	"time"
)

func GetPO() (*sql.Rows, error) {
	db := database.Connect()
	defer db.Close()

	return db.Query("SELECT * FROM purchase_order po ORDER BY po.id DESC")
}

func InsertPO(po PurchaseOrder.PurchaseOrder, person int) bool {
	db := database.Connect()
	defer db.Close()

	now := time.Now()

	_, err := db.Exec("INSERT INTO purchase_order(date, vendor_id, employee_id) VALUES ($1, $2, $3)",
		now,
		po.Vendor.ID,
		person,
	)

	if err != nil {
		return false
	}

	res, _ := db.Query("SELECT po.id FROM purchase_order po WHERE po.date = $1", now)

	var id int
	var scanId int

	for res.Next() {
		if err := res.Scan(&scanId); err != nil {
			log.Fatal(err.Error())
		} else {
			id = scanId
		}
	}

	var err2 error

	for i := 0; i < len(po.PurchaseDetails); i++ {
		_, err2 = db.Exec("INSERT INTO purchase_detail(price, quantity, material_id, purchase_order_id) VALUES ($1, $2, $3, $4)",
			po.PurchaseDetails[i].Price,
			po.PurchaseDetails[i].Quantity,
			po.PurchaseDetails[i].Material.ID,
			id,
		)
	}
	return err2 == nil
}

func InsertVendor(vendor External.External) bool {
	db := database.Connect()
	defer db.Close()

	_, err := db.Exec("INSERT INTO vendor(address, city, country, name, postalcode, region) VALUES ($1, $2, $3, $4, $5, $6)",
		vendor.Address,
		vendor.City,
		vendor.Country,
		vendor.Name,
		vendor.PostalCode,
		vendor.Region,
	)
	return err == nil
}

func GetAllVendor() []External.External {
	db := database.Connect()
	defer db.Close()

	res, _ := db.Query("SELECT * FROM vendor v")

	var vendor []External.External
	var vendorScan External.External

	for res.Next() {
		if err := res.Scan(
			&vendorScan.ID,
			&vendorScan.Address,
			&vendorScan.City,
			&vendorScan.Country,
			&vendorScan.Name,
			&vendorScan.PostalCode,
			&vendorScan.Region); err != nil {
			log.Fatal(err.Error())
		} else {
			vendor = append(vendor, vendorScan)
		}
	}

	return vendor

}

func GetVendorById(id int) External.External {
	db := database.Connect()
	defer db.Close()

	res, _ := db.Query("SELECT * FROM vendor v WHERE v.id = $1", id)

	var vendor External.External
	var vendorScan External.External

	for res.Next() {
		if err := res.Scan(
			&vendorScan.ID,
			&vendorScan.Address,
			&vendorScan.City,
			&vendorScan.Country,
			&vendorScan.Name,
			&vendorScan.PostalCode,
			&vendorScan.Region); err != nil {
			log.Fatal(err.Error())
		} else {
			vendor = vendorScan
		}
	}

	return vendor
}

func GetPurchaseDetails(po PurchaseOrder.PurchaseOrder) []PurchaseOrder.PurchaseDetail {
	db := database.Connect()
	defer db.Close()

	res, _ := db.Query("SELECT * FROM purchase_detail pd WHERE pd.purchase_order_id = $1", po.ID)

	var pd []PurchaseOrder.PurchaseDetail
	var scanPd PurchaseOrder.PurchaseDetail

	for res.Next() {
		if err := res.Scan(&scanPd.ID, &scanPd.Price, &scanPd.Quantity, &scanPd.Material.ID, &scanPd.PurchaseOrder.ID); err != nil {
			log.Fatal(err.Error())
		} else {
			scanPd.Material = ProductionRepository.GetMaterial(scanPd.Material.ID)
			pd = append(pd, scanPd)
		}
	}

	return pd
}

func RepoAddPurchaseInvoice(purchase_id string, employee_id string, date time.Time) bool {
	db := database.Connect()
	defer db.Close()

	purchase_id = "" + purchase_id
	temp_purchase_id, _ := strconv.Atoi(purchase_id)
	employee_id = "" + employee_id
	temp_employee_id, _ := strconv.Atoi(employee_id)

	_, errQuery := db.Exec("INSERT INTO invoice(date, sales_order_id, purchase_order_id, employee_id) VALUES($1,$2,$3,$4)",
		date,
		nil,
		temp_purchase_id,
		temp_employee_id,
	)

	fmt.Println("Masuk Line 107")
	if errQuery == nil {
		return true
	} else {
		fmt.Println(errQuery)
		return false
	}
}

func VerifyInvoiceStatus(po_id int) int {
	db := database.Connect()
	defer db.Close()

	res, _ := db.Query("SELECT COUNT(*) FROM invoice i WHERE i.purchase_order_id = $1", po_id)

	var id int
	var scanId int

	for res.Next() {
		if err := res.Scan(&scanId); err != nil {
			log.Fatal(err.Error())
		} else {
			id = scanId
		}
	}

	if id != 0 {
		return 1
	}
	return 0
}
