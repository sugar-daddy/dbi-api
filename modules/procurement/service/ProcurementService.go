package service

import (
	PurchaseOrder "dbi-api/modules/procurement/data"
	External "dbi-api/modules/sales/data"
	Response "dbi-api/responses"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	ProcurementRepository "dbi-api/modules/procurement/repository"

	"net/http"

	"github.com/gin-gonic/gin"
)

func GetPO(c *gin.Context) {
	rows, _ := ProcurementRepository.GetPO()

	var po []PurchaseOrder.PurchaseOrder
	var poScan PurchaseOrder.PurchaseOrder

	for rows.Next() {
		if err := rows.Scan(&poScan.ID, &poScan.Date, &poScan.Vendor.ID, &poScan.EmployeeID); err != nil {
			log.Fatal(err.Error())
		} else {
			po = append(po, poScan)
		}
	}

	for i := 0; i < len(po); i++ {
		po[i].Vendor = ProcurementRepository.GetVendorById(po[i].Vendor.ID)
		po[i].PurchaseDetails = ProcurementRepository.GetPurchaseDetails(po[i])
		po[i].Invoice_exist = ProcurementRepository.VerifyInvoiceStatus(po[i].ID)
	}

	var response Response.GeneralResponse

	if len(po) != 0 {
		response.Message = "OK"
		response.Status = 200
		response.Data = po
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Error"
		response.Status = 500
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, response)
	}
}

func AddPO(c *gin.Context) {
	vId, isFilledA := c.GetPostForm("vendor_id")
	price, isFilledB := c.GetPostForm("price")
	quantity, isFilledC := c.GetPostForm("quantity")
	mId, isFilledD := c.GetPostForm("material_id")
	employee_id, isFilledE := c.GetPostForm("employee_id")

	if !isFilledA {
		log.Fatal("Vendor id didn't entered")
	}

	if !isFilledB {
		log.Fatal("Price didn't entered")
	}

	if !isFilledC {
		log.Fatal("Quantity didn't entered")
	}

	if !isFilledD {
		log.Fatal("Material id didn't entered")
	}

	if !isFilledE {
		log.Fatal("Employee id didn't entered")
	}

	listPrice := strings.Split(price, ",")
	listQuantity := strings.Split(quantity, ",")
	listMaterialId := strings.Split(mId, ",")

	if !(len(listPrice) == len(listQuantity) && len(listQuantity) == len(listMaterialId)) {
		log.Fatal("Make sure the length of price, quantity, and material_id matches")
	}

	var po PurchaseOrder.PurchaseOrder
	var pd PurchaseOrder.PurchaseDetail

	for i := 0; i < len(listPrice); i++ {
		pd.Price, _ = strconv.Atoi(listPrice[i])
		pd.Quantity, _ = strconv.Atoi(listQuantity[i])
		pd.Material.ID, _ = strconv.Atoi(listMaterialId[i])

		po.Vendor.ID, _ = strconv.Atoi(vId)
		po.PurchaseDetails = append(po.PurchaseDetails, pd)
	}

	person, _ := strconv.Atoi(employee_id)

	res := ProcurementRepository.InsertPO(po, person)

	var response Response.GeneralResponse

	if res {
		response.Message = "OK"
		response.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Error"
		response.Status = 500
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, response)
	}
}

func AddVendor(c *gin.Context) {
	addr, isFilledA := c.GetPostForm("address")
	city, isFilledB := c.GetPostForm("city")
	country, isFilledC := c.GetPostForm("country")
	name, isFilledD := c.GetPostForm("name")
	pc, isFilledE := c.GetPostForm("postal-code")
	region, isFilledF := c.GetPostForm("region")

	if !isFilledA {
		log.Fatal("Vendor address didn't entered")
	}

	if !isFilledB {
		log.Fatal("Vendor city didn't entered")
	}

	if !isFilledC {
		log.Fatal("Vendor country didn't entered")
	}

	if !isFilledD {
		log.Fatal("Vendor name didn't entered")
	}

	if !isFilledE {
		log.Fatal("Vendor postal code didn't entered")
	}

	if !isFilledF {
		log.Fatal("Vendor region didn't entered")
	}

	var vendor External.External
	vendor.Address = addr
	vendor.City = city
	vendor.Country = country
	vendor.Name = name
	vendor.PostalCode = pc
	vendor.Region = region

	res := ProcurementRepository.InsertVendor(vendor)

	var response Response.GeneralResponse

	if res {
		response.Message = "OK"
		response.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Error"
		response.Status = 500
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, response)
	}
}

func GetVendor(c *gin.Context) {
	vendor := ProcurementRepository.GetAllVendor()

	var response Response.GeneralResponse

	if len(vendor) != 0 {
		response.Message = "OK"
		response.Status = 200
		response.Data = vendor
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Error"
		response.Status = 500
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, response)
	}
}

func AddPurchaseInvoice(c *gin.Context) {
	purchase_id := c.PostForm("purchase_id")
	employee_id := c.PostForm("employee_id")

	fmt.Println(purchase_id)

	success := ProcurementRepository.RepoAddPurchaseInvoice(purchase_id, employee_id, time.Now())

	var responses Response.GeneralResponse
	if success == true {
		responses.Message = "Success Get Purchase Invoice"
		responses.Status = 200
	} else {
		responses.Message = "Failed Get Purchase Invoice"
		responses.Status = 400
	}
	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusOK, responses)
}
