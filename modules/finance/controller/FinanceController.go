package controller

import (
	JWTService "dbi-api/modules/authentication/service"
	"dbi-api/modules/finance/service"

	"github.com/gin-gonic/gin"
)

func Routes(router *gin.Engine) {
	finance := router.Group("/")
	finance.Use(JWTService.Authenticate([]string{"admin", "finance"}))
	{
		finance.POST("/cash_report", service.AddCashReport)
		finance.GET("/cash_report", service.GetCashReport)
		finance.PUT("/cash_report", service.UpdateCashReport)
	}

}
