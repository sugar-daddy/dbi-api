package service

import (
	"net/http"
	"strconv"
	"time"

	Model "dbi-api/modules/finance/data"
	Repo "dbi-api/modules/finance/repository"

	response "dbi-api/responses"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func AddCashReport(c *gin.Context) {
	amount, _ := strconv.Atoi(c.PostForm("amount"))
	t := time.Now()
	descriptions := c.PostForm("descriptions")
	person, _ := strconv.Atoi(c.PostForm("employee_id"))
	status := c.PostForm("status")
	var CashReport Model.CashReport
	CashReport.Amount = amount
	CashReport.Date = t
	CashReport.Descriptions = descriptions
	CashReport.Person.ID = person
	CashReport.Status = status

	success := Repo.AddCashReport(CashReport)

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Add Cash Report"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Add Cash Report"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, responses)
	}

}

func GetCashReport(c *gin.Context) {

	ID := c.Param("cash_report_id")
	status := c.Query("status")

	CashReports := Repo.GetCashReport(ID, status)

	var responses response.GeneralResponse
	if len(CashReports) > 0 {
		responses.Message = "Success Get All Cash Reports"
		responses.Status = 200
		responses.Data = CashReports
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Get All Cash Reports"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, responses)
	}

}

func UpdateCashReport(c *gin.Context) {

	id := c.PostForm("id")
	amount, _ := strconv.Atoi(c.PostForm("amount"))
	descriptions := c.PostForm("descriptions")
	person, _ := strconv.Atoi(c.PostForm("employee_id"))
	status := c.PostForm("status")
	CashReport := Repo.GetCashReport(id, "")[0]

	if amount != 0 {
		CashReport.Amount = amount
	}

	if descriptions != "" {
		CashReport.Descriptions = descriptions
	}

	if person != 0 {
		CashReport.Person.ID = person
	}

	if status != "" {
		CashReport.Status = status
	}

	success := Repo.UpdateCashReport(CashReport)

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Update Cash Report"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Update Cash Report"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, responses)
	}

}
