package data

import (
	personal "dbi-api/modules/personal/data"
	"time"
)

type CashReport struct {
	ID           int               `form:"id" json:"id"`
	Date         time.Time         `form:"date" json:"date"`
	Status       string            `form:"status" json:"status"`
	Descriptions string            `form:"descriptions" json:"descriptions"`
	Amount       int               `form:"amount" json:"amount"`
	Person       personal.Employee `form:"person" json:"person"`
}

type CashReportResponse struct {
	Status  int          `form:"status" json:"status"`
	Message string       `form:"message" json:"message"`
	Data    []CashReport `form:"data" json:"data"`
}
