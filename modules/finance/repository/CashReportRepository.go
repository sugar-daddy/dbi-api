package repository

import (
	"dbi-api/database"
	Model "dbi-api/modules/finance/data"
	"log"

	_ "github.com/lib/pq"
)

func AddCashReport(CashReport Model.CashReport) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("INSERT INTO cash_report(amount, date, descriptions, employee_id, status)  VALUES($1, $2, $3, $4, $5)",
		CashReport.Amount,
		CashReport.Date,
		CashReport.Descriptions,
		CashReport.Person.ID,
		CashReport.Status,
	)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}

func GetCashReport(id string, status string) []Model.CashReport {
	db := database.Connect()
	defer db.Close()
	var query string

	if id != "" {
		query = "SELECT a.id, a.amount, a.date, a.descriptions, a.employee_id, a.status, b.address, b.birthday, b.city, b.country, b.email, b.education, b.name, b.part, b.region, b.username FROM cash_report a JOIN employee b ON a.employee_id = b.id WHERE a.id =" + id
	} else {
		query = "SELECT a.id, a.amount, a.date, a.descriptions, a.employee_id, a.status, b.address, b.birthday, b.city, b.country, b.email, b.education, b.name, b.part, b.region, b.username FROM cash_report a JOIN employee b ON a.employee_id = b.id"
	}

	if status != "" {
		query += " AND a.status ='" + status + "'"
	}

	query += " ORDER BY a.id DESC;"

	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}

	var CashReport Model.CashReport
	var CashReports []Model.CashReport

	for rows.Next() {
		if err := rows.Scan(&CashReport.ID, &CashReport.Amount, &CashReport.Date, &CashReport.Descriptions, &CashReport.Person.ID, &CashReport.Status, &CashReport.Person.Address, &CashReport.Person.BirthDay, &CashReport.Person.City, &CashReport.Person.Country, &CashReport.Person.Email, &CashReport.Person.Education, &CashReport.Person.Name, &CashReport.Person.Part, &CashReport.Person.Region, &CashReport.Person.Username); err != nil {
			log.Fatal(err.Error())
		} else {
			CashReports = append(CashReports, CashReport)
		}
	}
	return CashReports
}

func UpdateCashReport(CashReport Model.CashReport) bool {
	db := database.Connect()
	defer db.Close()

	_, errQuery := db.Exec("UPDATE cash_report SET amount = $1, descriptions = $2, employee_id = $3, status = $4 WHERE id = $5;",
		CashReport.Amount,
		CashReport.Descriptions,
		CashReport.Person.ID,
		CashReport.Status,
		CashReport.ID,
	)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}
