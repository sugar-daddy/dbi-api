package repository

import (
	"database/sql"
	"dbi-api/database"
)

func VerifyLogin(username, password string) (*sql.Rows, error) {
	db := database.Connect()
	defer db.Close()

	return db.Query("SELECT e.id, e.name, e.part FROM employee e WHERE e.username=$1 AND e.password=$2",
		username,
		password,
	)
}

func GetUserById(id int) (*sql.Rows, error) {
	db := database.Connect()
	defer db.Close()

	return db.Query("SELECT e.id, e.name, e.part FROM employee e WHERE e.id=$1",
		id,
	)
}
