package service

import (
	"dbi-api/config"
	"net/http"
	"time"

	Response "dbi-api/responses"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

var jwtKey = []byte(config.LoadConfig("JWT_KEY"))
var tokenName = "token"

type Claims struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	UserType string `json:"user_type"`
	jwt.StandardClaims
}

func generateToken(c *gin.Context, id int, name string, userType string) {
	tokenExpiryTime := time.Now().Add(5 * time.Minute)

	claims := &Claims{
		ID:       id,
		Name:     name,
		UserType: userType,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: tokenExpiryTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, err := token.SignedString(jwtKey)
	if err != nil {
		return
	}

	// c.SetCookie(tokenName, signedToken, tokenExpiryTime, "/", "localhost", false, true)
	c.SetCookie(tokenName, signedToken, 1000, "/", c.Request.Header.Get("Origin"), false, true)
}

func resetUserToken(c *gin.Context) {
	c.SetCookie(tokenName, "", -1, "/", c.Request.Header.Get("Origin"), false, true)
}

func Authenticate(accessTypes []string) gin.HandlerFunc {
	return func(c *gin.Context) {
		isValidToken := validateUserToken(c, accessTypes)
		if !isValidToken {
			var response Response.GeneralResponse
			response.Message = "Unauthorized"
			response.Status = 401
			c.Header("Content-Type", "application/json")
			c.JSON(http.StatusUnauthorized, response)
			c.Abort()
			return
		} else {
			c.Next()
		}
	}
}

func validateUserToken(c *gin.Context, accessTypes []string) bool {
	isAccessTokenValid, _, _, userType := ValidateTokenFromCookies(c)

	if isAccessTokenValid {
		isUserValid := false
		for _, accessType := range accessTypes {
			if userType == accessType {
				isUserValid = true
				break
			}
		}
		if isUserValid {
			return true
		}
	}
	return false
}

func ValidateTokenFromCookies(c *gin.Context) (bool, int, string, string) {
	if cookie, err := c.Cookie(tokenName); err == nil {
		accessToken := cookie
		accessClaims := &Claims{}
		parsedToken, err := jwt.ParseWithClaims(accessToken, accessClaims, func(accessToken *jwt.Token) (interface{}, error) {
			return jwtKey, nil
		})
		if err == nil && parsedToken.Valid {
			return true, accessClaims.ID, accessClaims.Name, accessClaims.UserType
		}
	}
	return false, -1, "", ""
}
