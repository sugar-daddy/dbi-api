package service

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"

	AuthenticationRepository "dbi-api/modules/authentication/repository"
	Employee "dbi-api/modules/personal/data"
	Response "dbi-api/responses"
)

func Login(c *gin.Context) {
	username := c.PostForm("username")
	password := c.PostForm("password")

	rows, _ := AuthenticationRepository.VerifyLogin(username, password)

	var employee Employee.Employee
	var employeeScan Employee.Employee

	for rows.Next() {
		if err := rows.Scan(&employeeScan.ID, &employeeScan.Name, &employeeScan.Part); err != nil {
			log.Fatal(err.Error())
		} else {
			employee = employeeScan
		}
	}

	var response Response.GeneralResponse

	if employee != (Employee.Employee{}) {
		generateToken(c, employee.ID, employee.Name, employee.Part)
		response.Message = "OK"
		response.Status = 200
		response.Data = employee
		c.Header("Content-Type", "application/json")
		c.Writer.Header().Set("Access-Control-Expose-Headers", "*")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Unauthorized"
		response.Status = 401
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, response)
	}
}

func Logout(c *gin.Context) {
	resetUserToken(c)

	var response Response.GeneralResponse
	response.Message = "OK"
	response.Status = 200
	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusOK, response)
}

func VerifyCookies(c *gin.Context) {
	result, id, _, _ := ValidateTokenFromCookies(c)

	rows, _ := AuthenticationRepository.GetUserById(id)

	var employee Employee.Employee
	var employeeScan Employee.Employee

	for rows.Next() {
		if err := rows.Scan(&employeeScan.ID, &employeeScan.Name, &employeeScan.Part); err != nil {
			log.Fatal(err.Error())
		} else {
			employee = employeeScan
		}
	}

	var response Response.GeneralResponse

	if result {
		resetUserToken(c)
		generateToken(c, employee.ID, employee.Name, employee.Part)
		response.Message = "Accepted"
		response.Status = 202
		response.Data = employee
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusAccepted, response)
	} else {
		response.Message = "Unauthorized"
		response.Status = 401
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, response)
	}
}
