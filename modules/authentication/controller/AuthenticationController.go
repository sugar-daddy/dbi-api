package controller

import (
	"github.com/gin-gonic/gin"

	AuthenticationService "dbi-api/modules/authentication/service"
)

func Routes(router *gin.Engine) {
	router.POST("/login", AuthenticationService.Login)
	router.POST("/logout", AuthenticationService.Logout)
	router.GET("/verify", AuthenticationService.VerifyCookies)
}
