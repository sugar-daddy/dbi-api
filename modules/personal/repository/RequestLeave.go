package repository

import (
	"dbi-api/database"
	Model "dbi-api/modules/personal/data"
	"log"
	"strconv"

	_ "github.com/lib/pq"
)

func AddRequestLeave(RequestLeave Model.RequestLeave) bool {
	db := database.Connect()
	defer db.Close()

	_, errQuery := db.Exec("INSERT INTO request_leave (end_date, is_accepted, start_date, type, employee_id) VALUES($1,$2,$3,$4,$5)",
		RequestLeave.EndDate,
		RequestLeave.IsAccepted,
		RequestLeave.StartDate,
		RequestLeave.Type,
		RequestLeave.Person.ID,
	)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}

func GetRequestLeave(id string) []Model.RequestLeave {
	db := database.Connect()
	defer db.Close()
	var query string

	if id != "" {
		query = "SELECT * FROM request_leave where id = " + id + ";"
	} else {
		query = "SELECT * FROM request_leave ORDER BY id DESC;"
	}

	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}

	var RequestLeave Model.RequestLeave
	var RequestLeaves []Model.RequestLeave

	for rows.Next() {
		if err := rows.Scan(&RequestLeave.ID, &RequestLeave.EndDate, &RequestLeave.IsAccepted, &RequestLeave.StartDate, &RequestLeave.Type, &RequestLeave.Person.ID); err != nil {
			log.Fatal(err.Error())
		} else {
			RequestLeave.Person = GetEmployee(strconv.Itoa(RequestLeave.Person.ID))[0]
			RequestLeaves = append(RequestLeaves, RequestLeave)
		}
	}

	return RequestLeaves
}

func UpdateRequestLeave(RequestLeave Model.RequestLeave) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("UPDATE request_leave SET end_date = $1, is_accepted = $2, start_date = $3, type = $4, employee_id = $5 WHERE id = $6",
		RequestLeave.EndDate,
		RequestLeave.IsAccepted,
		RequestLeave.StartDate,
		RequestLeave.Type,
		RequestLeave.Person.ID,
		RequestLeave.ID,
	)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}
