package repository

import (
	"dbi-api/database"
	Model "dbi-api/modules/personal/data"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

func AddEmployee(Employee Model.Employee) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("INSERT INTO employee(address, birthday, city, country, email, education, name, part, password, region, username, salary, monday_in, monday_out, tuesday_in, tuesday_out, wednesday_in, wednesday_out, thursday_in, thursday_out, friday_in, friday_out) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22)",
		Employee.Address,
		Employee.BirthDay,
		Employee.City,
		Employee.Country,
		Employee.Email,
		Employee.Education,
		Employee.Name,
		Employee.Part,
		Employee.Password,
		Employee.Region,
		Employee.Username,
		Employee.Salary,
		Employee.MondayIn,
		Employee.MondayOut,
		Employee.TuesdayIn,
		Employee.TuesdayOut,
		Employee.WednesdayIn,
		Employee.WednesdayOut,
		Employee.ThursdayIn,
		Employee.ThursdayOut,
		Employee.FridayIn,
		Employee.FridayOut,
	)

	if errQuery == nil {
		return true
	} else {
		fmt.Println(errQuery)
		return false
	}
}

func GetEmployee(id string) []Model.Employee {
	db := database.Connect()
	defer db.Close()
	var query string

	if id != "" {
		query = "SELECT id, address, birthday, city, country, email, education, name, part, password, region, username, salary, monday_in, monday_out, tuesday_in, tuesday_out, wednesday_in, wednesday_out, thursday_in, thursday_out, friday_in, friday_out FROM employee where id = " + id + ";"
	} else {
		query = "SELECT id, address, birthday, city, country, email, education, name, part, password, region, username, salary, monday_in, monday_out, tuesday_in, tuesday_out, wednesday_in, wednesday_out, thursday_in, thursday_out, friday_in, friday_out FROM employee ORDER BY id DESC;"
	}

	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}

	var Employee Model.Employee
	var Employees []Model.Employee

	for rows.Next() {
		if err := rows.Scan(&Employee.ID, &Employee.Address, &Employee.BirthDay, &Employee.City, &Employee.Country, &Employee.Email, &Employee.Education, &Employee.Name, &Employee.Part, &Employee.Password, &Employee.Region, &Employee.Username, &Employee.Salary, &Employee.MondayIn, &Employee.MondayOut, &Employee.TuesdayIn, &Employee.TuesdayOut, &Employee.WednesdayIn, &Employee.WednesdayOut, &Employee.ThursdayIn, &Employee.ThursdayOut, &Employee.FridayIn, &Employee.FridayOut); err != nil {
			log.Fatal(err.Error())
		} else {
			Employees = append(Employees, Employee)
		}
	}

	return Employees
}

func UpdateEmployee(Employee Model.Employee) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("UPDATE employee SET address = $1, birthday = $2, city = $3, country = $4, email = $5, education = $6, name = $7, part = $8, password = $9, region = $10, username = $11, salary = $12 , monday_in = $13, monday_out = $14, tuesday_in = $15, tuesday_out = $16, wednesday_in = $17, wednesday_out = $18, thursday_in = $19, thursday_out = $20, friday_in = $21, friday_out = $22 WHERE id = $23;",
		Employee.Address,
		Employee.BirthDay,
		Employee.City,
		Employee.Country,
		Employee.Email,
		Employee.Education,
		Employee.Name,
		Employee.Part,
		Employee.Password,
		Employee.Region,
		Employee.Username,
		Employee.Salary,
		Employee.MondayIn,
		Employee.MondayOut,
		Employee.TuesdayIn,
		Employee.TuesdayOut,
		Employee.WednesdayIn,
		Employee.WednesdayOut,
		Employee.ThursdayIn,
		Employee.ThursdayOut,
		Employee.FridayIn,
		Employee.FridayOut,
		Employee.ID,
	)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}
