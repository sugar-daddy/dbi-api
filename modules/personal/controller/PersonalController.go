package controller

import (
	JWTService "dbi-api/modules/authentication/service"
	"dbi-api/modules/personal/service"

	"github.com/gin-gonic/gin"
)

func Routes(router *gin.Engine) {

	router.POST("/request_leave", service.AddRequestLeave)

	personal := router.Group("/")
	personal.Use(JWTService.Authenticate([]string{"admin"}))
	{
		personal.POST("/employee", service.AddEmployee)
		personal.GET("/employee", service.GetEmployee)
		personal.PUT("/employee", service.UpdateEmployee)

		personal.GET("/request_leave", service.GetRequestLeave)
		personal.PUT("/request_leave", service.UpdateRequestLeave)
	}
}
