package service

import (
	"net/http"
	"strconv"
	"time"

	Model "dbi-api/modules/personal/data"
	Repo "dbi-api/modules/personal/repository"

	response "dbi-api/responses"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func AddEmployee(c *gin.Context) {
	address := c.PostForm("address")
	birthday := c.PostForm("birthday")
	t, _ := time.Parse("2006-01-02", birthday)
	city := c.PostForm("city")
	country := c.PostForm("country")
	email := c.PostForm("email")
	education := c.PostForm("education")
	name := c.PostForm("name")
	part := c.PostForm("part")
	password := c.PostForm("password")
	region := c.PostForm("region")
	username := c.PostForm("username")
	salary, _ := strconv.Atoi(c.PostForm("salary"))
	mondayIn := c.PostForm("monday_in")
	mi, _ := time.Parse("15:04:05", string(mondayIn))
	mondayOut := c.PostForm("monday_out")
	mo, _ := time.Parse("15:04:05", string(mondayOut))
	tuesdayIn := c.PostForm("tuesday_in")
	ti, _ := time.Parse("15:04:05", string(tuesdayIn))
	tuesdayOut := c.PostForm("tuesday_out")
	to, _ := time.Parse("15:04:05", string(tuesdayOut))
	wednesdayIn := c.PostForm("wednesday_in")
	wi, _ := time.Parse("15:04:05", string(wednesdayIn))
	wednesdayOut := c.PostForm("wednesday_out")
	wo, _ := time.Parse("15:04:05", string(wednesdayOut))
	thursdayIn := c.PostForm("thursday_in")
	tri, _ := time.Parse("15:04:05", string(thursdayIn))
	thursdayOut := c.PostForm("thursday_out")
	tro, _ := time.Parse("15:04:05", string(thursdayOut))
	fridayIn := c.PostForm("friday_in")
	fi, _ := time.Parse("15:04:05", string(fridayIn))
	fridayOut := c.PostForm("friday_out")
	fo, _ := time.Parse("15:04:05", string(fridayOut))

	var Employee Model.Employee
	Employee.Address = address
	Employee.BirthDay = t
	Employee.City = city
	Employee.Country = country
	Employee.Email = email
	Employee.Education = education
	Employee.Name = name
	Employee.Part = part
	Employee.Password = password
	Employee.Region = region
	Employee.Username = username
	Employee.Salary = salary
	Employee.MondayIn = mi
	Employee.MondayOut = mo
	Employee.TuesdayIn = ti
	Employee.TuesdayOut = to
	Employee.WednesdayIn = wi
	Employee.WednesdayOut = wo
	Employee.ThursdayIn = tri
	Employee.ThursdayOut = tro
	Employee.FridayIn = fi
	Employee.FridayOut = fo

	success := Repo.AddEmployee(Employee)

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Add Employee"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Add Employee"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}

}

func GetEmployee(c *gin.Context) {

	Employees := Repo.GetEmployee("")

	var responses response.GeneralResponse
	if len(Employees) > 0 {
		responses.Message = "Success Get All Employees"
		responses.Status = 200
		responses.Data = Employees
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Get All Employees"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}

func UpdateEmployee(c *gin.Context) {

	id := c.PostForm("id")
	address := c.PostForm("address")
	birthday := c.PostForm("birthday")
	t, _ := time.Parse("2006-01-02", birthday)
	city := c.PostForm("city")
	country := c.PostForm("country")
	email := c.PostForm("email")
	education := c.PostForm("education")
	name := c.PostForm("name")
	part := c.PostForm("part")
	password := c.PostForm("password")
	region := c.PostForm("region")
	username := c.PostForm("username")
	salary, _ := strconv.Atoi(c.PostForm("salary"))
	mondayIn := c.PostForm("monday_in")
	mi, _ := time.Parse("15:04:05", string(mondayIn))
	mondayOut := c.PostForm("monday_out")
	mo, _ := time.Parse("15:04:05", string(mondayOut))
	tuesdayIn := c.PostForm("tuesday_in")
	ti, _ := time.Parse("15:04:05", string(tuesdayIn))
	tuesdayOut := c.PostForm("tuesday_out")
	to, _ := time.Parse("15:04:05", string(tuesdayOut))
	wednesdayIn := c.PostForm("wednesday_in")
	wi, _ := time.Parse("15:04:05", string(wednesdayIn))
	wednesdayOut := c.PostForm("wednesday_out")
	wo, _ := time.Parse("15:04:05", string(wednesdayOut))
	thursdayIn := c.PostForm("thursday_in")
	tri, _ := time.Parse("15:04:05", string(thursdayIn))
	thursdayOut := c.PostForm("thursday_out")
	tro, _ := time.Parse("15:04:05", string(thursdayOut))
	fridayIn := c.PostForm("friday_in")
	fi, _ := time.Parse("15:04:05", string(fridayIn))
	fridayOut := c.PostForm("friday_out")
	fo, _ := time.Parse("15:04:05", string(fridayOut))
	Employee := Repo.GetEmployee(id)[0]

	if address != "" {
		Employee.Address = address
	}

	if birthday != "" {
		Employee.BirthDay = t
	}

	if city != "" {
		Employee.City = city
	}

	if country != "" {
		Employee.Country = country
	}

	if email != "" {
		Employee.Email = email
	}

	if education != "" {
		Employee.Education = education
	}

	if name != "" {
		Employee.Name = name
	}

	if part != "" {
		Employee.Part = part
	}

	if password != "" {
		Employee.Password = password
	}

	if region != "" {
		Employee.Region = region
	}

	if username != "" {
		Employee.Username = username
	}

	if salary != 0 {
		Employee.Salary = salary
	}

	if mondayIn != "" {
		Employee.MondayIn = mi
	}

	if mondayOut != "" {
		Employee.MondayOut = mo
	}

	if tuesdayIn != "" {
		Employee.TuesdayIn = ti
	}

	if tuesdayOut != "" {
		Employee.TuesdayOut = to
	}

	if wednesdayIn != "" {
		Employee.WednesdayIn = wi
	}

	if wednesdayOut != "" {
		Employee.WednesdayOut = wo
	}

	if thursdayIn != "" {
		Employee.ThursdayIn = tri
	}

	if thursdayOut != "" {
		Employee.ThursdayOut = tro
	}

	if fridayIn != "" {
		Employee.FridayIn = fi
	}

	if fridayOut != "" {
		Employee.FridayOut = fo
	}

	success := Repo.UpdateEmployee(Employee)

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Update Employee"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Update Employee"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}

}
