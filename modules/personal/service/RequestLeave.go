package service

import (
	"net/http"
	"strconv"
	"time"

	Model "dbi-api/modules/personal/data"
	Repo "dbi-api/modules/personal/repository"

	response "dbi-api/responses"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func AddRequestLeave(c *gin.Context) {
	end_date := c.PostForm("end_date")
	t, _ := time.Parse("2006-01-02", end_date)
	is_accepted, _ := strconv.Atoi(c.PostForm("is_accepted"))
	start_date := c.PostForm("start_date")
	t2, _ := time.Parse("2006-01-02", start_date)
	tipe := c.PostForm("type")
	person, _ := strconv.Atoi(c.PostForm("employee_id"))
	var RequestLeave Model.RequestLeave
	RequestLeave.EndDate = t
	RequestLeave.IsAccepted = is_accepted
	RequestLeave.StartDate = t2
	RequestLeave.Type = tipe
	RequestLeave.Person.ID = person

	success := Repo.AddRequestLeave(RequestLeave)

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Add Request Leave"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Add Request Leave"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}

}

func GetRequestLeave(c *gin.Context) {

	RequestLeaves := Repo.GetRequestLeave("")

	var responses response.GeneralResponse
	if len(RequestLeaves) > 0 {
		responses.Message = "Success Get All RequestLeaves"
		responses.Status = 200
		responses.Data = RequestLeaves
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Get All RequestLeaves"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}

}

func UpdateRequestLeave(c *gin.Context) {

	id := c.PostForm("id")
	end_date := c.PostForm("end_date")
	t, _ := time.Parse(end_date, "2016-06-10")
	is_accepted, _ := strconv.Atoi(c.PostForm("is_accepted"))
	tipe := c.PostForm("type")
	person, _ := strconv.Atoi(c.PostForm("employee_id"))
	RequestLeave := Repo.GetRequestLeave(id)[0]

	if end_date != "" {
		RequestLeave.EndDate = t
	}

	if is_accepted != 0 {
		RequestLeave.IsAccepted = is_accepted
	}

	if tipe != "" {
		RequestLeave.Type = tipe
	}

	if person != 0 {
		RequestLeave.Person.ID = person
	}

	success := Repo.UpdateRequestLeave(RequestLeave)

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Update Request Leave"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Update Request Leave"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}

}
