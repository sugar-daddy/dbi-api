package data

import "time"

type Employee struct {
	ID           int       `form:"id" json:"id"`
	Name         string    `form:"name" json:"name"`
	Email        string    `form:"email" json:"email"`
	BirthDay     time.Time `form:"birthday" json:"birthday"`
	Education    string    `form:"education" json:"education"`
	Part         string    `form:"part" json:"part"`
	Address      string    `form:"address" json:"address"`
	Country      string    `form:"country" json:"country"`
	Region       string    `form:"region" json:"region"`
	City         string    `form:"city" json:"city"`
	Username     string    `form:"username" json:"username"`
	Password     string    `form:"password" json:"password"`
	Salary       int       `form:"salary" json:"salary"`
	MondayIn     time.Time `form:"monday_in" json:"monday_in"`
	MondayOut    time.Time `form:"monday_out" json:"monday_out"`
	TuesdayIn    time.Time `form:"tuesday_in" json:"tuesday_in"`
	TuesdayOut   time.Time `form:"tuesday_out" json:"tuesday_out"`
	WednesdayIn  time.Time `form:"wednesday_in" json:"wednesday_in"`
	WednesdayOut time.Time `form:"wednesday_out" json:"wednesday_out"`
	ThursdayIn   time.Time `form:"thursday_in" json:"thursday_in"`
	ThursdayOut  time.Time `form:"thursday_out" json:"thursday_out"`
	FridayIn     time.Time `form:"friday_in" json:"friday_in"`
	FridayOut    time.Time `form:"friday_out" json:"friday_out"`
}

type EmployeeResponse struct {
	Status  int        `form:"status" json:"status"`
	Message string     `form:"message" json:"message"`
	Data    []Employee `form:"data" json:"data"`
}
