package data

import "time"

type RequestLeave struct {
	ID         int       `form:"id" json:"id"`
	Person     Employee  `form:"person" json:"person"`
	Type       string    `form:"type" json:"type"`
	StartDate  time.Time `form:"startdate" json:"startdate"`
	EndDate    time.Time `form:"enddate" json:"enddate"`
	IsAccepted int       `form:"isaccepted" json:"isaccepted"`
}

type RequestLeaveResponse struct {
	Status  int            `form:"status" json:"status"`
	Message string         `form:"message" json:"message"`
	Data    []RequestLeave `form:"data" json:"data"`
}
