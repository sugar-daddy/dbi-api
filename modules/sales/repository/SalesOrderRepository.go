package repository

import (
	"dbi-api/database"
	dto "dbi-api/modules/sales/data/dto"
	"fmt"
	"log"
	"strconv"
	"time"

	_ "github.com/lib/pq"
)

func AddSalesOrder(customer_id string, employee_id string) bool {
	db := database.Connect()
	defer db.Close()

	date := time.Now()
	_, errQuery := db.Exec("INSERT INTO sales_order(date, customer_id, employee_id) VALUES($1,$2,$3)",
		date,
		customer_id,
		employee_id,
	)
	fmt.Println("Line 23 - SalesRepo")
	if errQuery == nil {
		return true
	} else {
		fmt.Println(errQuery)
		return false
	}
}

func GetSalesOrder(id string, is_accepted string) []dto.SalesOrderDTO {
	db := database.Connect()
	defer db.Close()
	query := "SELECT a.*, b.is_accepted FROM sales_order a JOIN request_material b ON a.id = b.sales_id"

	if id != "" {
		query += " where a.id = " + id
	}

	if is_accepted != "" {
		query += " where b.is_accepted = " + is_accepted
	}

	query += " ORDER BY id DESC;"

	// fmt.Println(query)
	query += ";"

	rows, err := db.Query(query)
	if err != nil {
		fmt.Println(err)
	}

	// len(rows)

	var SalesOrder dto.SalesOrderDTO
	var SalesOrders []dto.SalesOrderDTO

	var count int
	tempCount := 0

	for rows.Next() {
		if err := rows.Scan(&SalesOrder.ID, &SalesOrder.Date, &SalesOrder.Customer.ID, &SalesOrder.EmployeeID, &SalesOrder.IsAccepted); err != nil {
			log.Fatal(err.Error())
		} else {
			err2 := db.QueryRow("SELECT COUNT(*) FROM invoice WHERE sales_order_id = " + strconv.Itoa(SalesOrder.ID)).Scan(&count)

			tempCount++
			if err2 != nil {
				fmt.Println(err2)
			}
			SalesOrder.InvoiceExist = count

			SalesOrders = append(SalesOrders, SalesOrder)
		}
	}

	for i := 0; i < len(SalesOrders); i++ {
		SalesOrders[i].SalesDetails = GetSalesOrderDetail(strconv.Itoa(SalesOrders[i].ID))
		SalesOrders[i].Customer = GetCustomer(strconv.Itoa(SalesOrders[i].Customer.ID))[0]
	}

	return SalesOrders
}

func DeleteSalesOrder(id string) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("DELETE FROM sales_order WHERE id = " + id)
	if errQuery == nil {
		return true
	} else {
		return false
	}
}

func GetLastSalesOrderID() int {
	db := database.Connect()
	defer db.Close()

	var sales_order int
	query := "SELECT MAX(id) FROM sales_order;"
	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}
	for rows.Next() {
		if err := rows.Scan(&sales_order); err != nil {
			log.Fatal(err.Error())
		}
	}

	return sales_order
}

func RepoAddSalesInvoice(sales_id string, employee_id string, date time.Time) bool {
	db := database.Connect()
	defer db.Close()

	sales_id = "" + sales_id
	temp_sales_id, _ := strconv.Atoi(sales_id)
	employee_id = "" + employee_id
	temp_employee_id, _ := strconv.Atoi(employee_id)

	_, errQuery := db.Exec("INSERT INTO invoice(date, sales_order_id, purchase_order_id, employee_id) VALUES($1,$2,$3,$4)",
		date,
		temp_sales_id,
		nil,
		temp_employee_id,
	)

	if errQuery == nil {
		return true
	} else {
		fmt.Println(errQuery)
		return false
	}
}

func RepoGetSalesInvoice(id string) []dto.SalesInvoice {
	db := database.Connect()
	defer db.Close()
	query := "SELECT * FROM invoice WHERE sales_order_id IS NOT NULL"

	if id != "" {
		query += " AND id = " + id
	}

	query += " ORDER BY id DESC;"

	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}

	var SalesInvoice dto.SalesInvoice
	var SalesInvoices []dto.SalesInvoice
	var temp_id int
	var temp *int

	for rows.Next() {
		if err := rows.Scan(&SalesInvoice.ID, &SalesInvoice.Date, &temp_id, &temp, &SalesInvoice.EmployeeID); err != nil {
			log.Fatal(err.Error())
		} else {
			SalesInvoice.SalesOrder = GetSalesOrder(strconv.Itoa(temp_id), "")[0]
			SalesInvoices = append(SalesInvoices, SalesInvoice)
		}
	}

	return SalesInvoices
}
