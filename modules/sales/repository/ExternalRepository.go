package repository

import (
	"dbi-api/database"
	Model "dbi-api/modules/sales/data"
	"log"

	_ "github.com/lib/pq"
)

func AddCustomer(Customer Model.External) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("INSERT INTO customer(address, city, country, name, postalcode, region) VALUES($1,$2,$3,$4,$5,$6)",
		Customer.Address,
		Customer.City,
		Customer.Country,
		Customer.Name,
		Customer.PostalCode,
		Customer.Region,
	)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}

func GetCustomer(id string) []Model.External {
	db := database.Connect()
	defer db.Close()
	query := "SELECT * FROM customer"

	if id != "" {
		query += " where id = " + id
	}

	query += " ORDER BY id DESC;"

	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}

	var Customer Model.External
	var Customers []Model.External

	for rows.Next() {
		if err := rows.Scan(&Customer.ID, &Customer.Address, &Customer.City,
			&Customer.Country, &Customer.Name, &Customer.PostalCode, &Customer.Region); err != nil {
			log.Fatal(err.Error())
		} else {
			Customers = append(Customers, Customer)
		}
	}

	return Customers
}

func UpdateCustomer(Customer Model.External) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("UPDATE customer SET address = $1, city = $2, country = $3, name = $4, postalcode = $5, region = $6 WHERE id = $7;",
		Customer.Address,
		Customer.City,
		Customer.Country,
		Customer.Name,
		Customer.PostalCode,
		Customer.Region,
		Customer.ID,
	)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}

func DeleteCustomer(id string) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("DELETE FROM customer WHERE id = " + id)
	if errQuery == nil {
		return true
	} else {
		return false
	}
}
