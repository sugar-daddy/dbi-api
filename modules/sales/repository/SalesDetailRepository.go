package repository

import (
	"dbi-api/database"
	Model "dbi-api/modules/sales/data"
	"fmt"
	"log"
	"strconv"
	"strings"

	_ "github.com/lib/pq"
)

func Quantity(material_id string, quantity string) bool {
	db := database.Connect()
	defer db.Close()
	// query := "UPDATE request_material"

	listMaterialId := strings.Split(material_id, ",")
	listQuantity := strings.Split(quantity, ",")

	var temp int

	var listQuantityTemp []int
	success := true

	for i := 0; i < len(listMaterialId); i++ {
		query := "SELECT quantity FROM material where id = " + listMaterialId[i] + ";"
		rows, err := db.Query(query)
		if err != nil {
			log.Println(err)
		}
		for rows.Next() {
			if err := rows.Scan(&temp); err != nil {
				log.Fatal(err.Error())
			}
		}

		temp2, _ := strconv.Atoi(listQuantity[i])

		if temp < temp2 {
			success = false
		} else {
			temp = temp - temp2
			listQuantityTemp = append(listQuantityTemp, temp)
		}
	}
	successupdate := true

	if success == false {
		return false
	} else {
		for i := 0; i < len(listQuantity); i++ {
			_, errQuery := db.Exec("UPDATE material SET quantity=$1 WHERE id=$2",
				listQuantityTemp[i],
				listMaterialId[i],
			)
			if errQuery != nil {
				fmt.Println(errQuery)
				successupdate = false
			}
		}
	}
	if successupdate == true {
		return true
	} else {
		return false
	}
}

func AddSalesDetail(price string, quantity string, material_id string) bool {
	db := database.Connect()
	defer db.Close()

	var sales_id string
	query := "SELECT MAX(id) FROM sales_order;"
	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}
	for rows.Next() {
		if err := rows.Scan(&sales_id); err != nil {
			log.Fatal(err.Error())
		}
	}

	arrPrice := strings.Split(price, ",")
	arrquantity := strings.Split(quantity, ",")
	arrmaterial_id := strings.Split(material_id, ",")

	for i := 0; i < len(arrPrice); i++ {
		strings, _ := strconv.Atoi(string(arrmaterial_id[i]))
		_, errQuery := db.Exec("INSERT INTO sales_detail(price, quantity, sales_order_id, material_id) VALUES($1,$2,$3,$4)",
			arrPrice[i],
			arrquantity[i],
			sales_id,
			strings,
		)
		if errQuery != nil {
			fmt.Println(errQuery)
			return false
		}
	}

	return true
}

func GetSalesOrderDetail(sales_order_id string) []Model.SalesDetail {
	db := database.Connect()
	defer db.Close()
	query := "SELECT a.id, a.price, a.quantity, b.id, b.measurement, b.name, b.price, b.quantity, b.type FROM sales_detail a JOIN material b ON a.material_id = b.id"

	if sales_order_id != "" {
		query += " where sales_order_id = " + sales_order_id
	}

	query += ";"

	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}

	var SalesDetail Model.SalesDetail
	var SalesDetails []Model.SalesDetail

	for rows.Next() {
		if err := rows.Scan(&SalesDetail.ID, &SalesDetail.Price, &SalesDetail.Quantity, &SalesDetail.Material.ID,
			&SalesDetail.Material.Measurement, &SalesDetail.Material.Name, &SalesDetail.Material.Price,
			&SalesDetail.Material.Quantity, &SalesDetail.Material.Type); err != nil {
			log.Fatal(err.Error())
		} else {
			SalesDetails = append(SalesDetails, SalesDetail)
		}
	}

	return SalesDetails
}

func DeleteSalesDetail(id string) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("DELETE FROM sales_detail WHERE sales_order_id = " + id)
	if errQuery == nil {
		return true
	} else {
		return false
	}
}
