package data

import inventory "dbi-api/modules/inventory/data"

type SalesDetail struct {
	ID       int                `form:"id" json:"id"`
	Material inventory.Material `form:"material" json:"material"`
	Quantity int                `form:"quantity" json:"quantity"`
	Price    int                `form:"price" json:"price"`
}

type SalesDetailResponse struct {
	Status  int           `form:"status" json:"status"`
	Message string        `form:"message" json:"message"`
	Data    []SalesDetail `form:"data" json:"data"`
}
