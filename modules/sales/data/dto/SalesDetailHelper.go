package dto

type SalesDetailHelper struct {
	ID           int `form:"id" json:"id"`
	Price        int `form:"price" json:"price"`
	Quantity     int `form:"quantity" json:"quantity"`
	SalesOrderID int `form:"sales_order_id" json:"sales_order_id"`
	MaterialID   int `form:"material_id" json:"material_id"`
}
