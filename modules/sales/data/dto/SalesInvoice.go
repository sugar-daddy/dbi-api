package dto

import "time"

type SalesInvoice struct {
	ID         int           `form:"id" json:"id"`
	Date       time.Time     `form:"date" json:"date"`
	SalesOrder SalesOrderDTO `form:"salesorder" json:"salesorder"`
	EmployeeID string        `form:"employee_id" json:"employee_id"`
}
