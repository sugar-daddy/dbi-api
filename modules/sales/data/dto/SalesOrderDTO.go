package dto

import (
	Model "dbi-api/modules/sales/data"
	"time"
)

type SalesOrderDTO struct {
	ID           int                 `form:"id" json:"id"`
	Date         time.Time           `form:"date" json:"date"`
	Customer     Model.External      `form:"customer" json:"customer"`
	SalesDetails []Model.SalesDetail `form:"salesdetails" json:"salesdetails"`
	EmployeeID   *int                `form:"employeeid" json:"employeeid"`
	IsAccepted   int                 `form:"is_accepted" json:"is_accepted"`
	InvoiceExist int                 `form:"invoice_exist" json:"invoice_exist"`
}
