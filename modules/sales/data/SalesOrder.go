package data

import "time"

type SalesOrder struct {
	ID           int           `form:"id" json:"id"`
	Date         time.Time     `form:"date" json:"date"`
	Customer     External      `form:"customer" json:"customer"`
	SalesDetails []SalesDetail `form:"salesdetails" json:"salesdetails"`
	IsAccepted   int           `form:"is_accepted" json:"is_accepted"`
}

type SalesOrderResponse struct {
	Status  int          `form:"status" json:"status"`
	Message string       `form:"message" json:"message"`
	Data    []SalesOrder `form:"data" json:"data"`
}
