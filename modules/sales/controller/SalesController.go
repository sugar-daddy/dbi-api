package controller

import (
	JWTService "dbi-api/modules/authentication/service"
	"dbi-api/modules/sales/service"

	"github.com/gin-gonic/gin"
)

func Routes(router *gin.Engine) {
	sales := router.Group("/")
	sales.Use(JWTService.Authenticate([]string{"admin", "sales"}))
	{
		sales.POST("/customer", service.AddCustomer)
		sales.GET("/customer/:customer_id", service.GetCustomer)
		sales.GET("/customer", service.GetCustomer)
		sales.PUT("/customer", service.UpdateCustomer)
		sales.POST("/sales_order", service.AddSalesOrder)
		sales.GET("/sales_invoice", service.GetSalesInvoice)
		sales.GET("/sales_invoice/:id", service.GetSalesInvoice)
	}

	sales2 := router.Group("/")
	sales2.Use(JWTService.Authenticate([]string{"admin", "finance"}))
	{
		sales2.POST("/sales_invoice", service.AddSalesInvoice)
	}

	sales3 := router.Group("/")
	sales3.Use(JWTService.Authenticate([]string{"admin", "sales", "finance"}))
	{
		sales3.GET("/sales_order", service.GetSalesOrder)
		sales3.GET("/sales_order/:sales_order_id", service.GetSalesOrder)
	}
}
