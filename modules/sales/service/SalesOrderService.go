package service

import (
	Model "dbi-api/modules/inventory/data"
	service "dbi-api/modules/inventory/service"
	Repo "dbi-api/modules/sales/repository"
	response "dbi-api/responses"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func AddSalesOrder(c *gin.Context) {
	customer_id := c.PostForm("customer_id")
	price := c.PostForm("price")
	quantity := c.PostForm("quantity")
	material_id := c.PostForm("material_id")
	var RequestMaterial Model.RequestMaterials
	t := time.Now()
	RequestMaterial.Date = t
	RequestMaterial.Type = "Finished Material"
	employee_id := c.PostForm("employee_id")
	employee_id_int, _ := strconv.Atoi(employee_id)

	success := Repo.AddSalesOrder(customer_id, employee_id)
	salesId := Repo.GetLastSalesOrderID()

	success = service.AddRequestMaterialForSales(employee_id_int, quantity, material_id, strconv.Itoa(salesId))
	if success == true {
		success = Repo.AddSalesDetail(price, quantity, material_id)
	}

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Add Sales Order"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Add Sales Order"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}

func GetSalesOrder(c *gin.Context) {
	ID := c.Param("sales_order_id")
	is_accepted := c.Query("is_accepted")

	Materials := Repo.GetSalesOrder(ID, is_accepted)

	var responses response.GeneralResponse
	if len(Materials) > 0 {
		responses.Message = "Success Get Sales Order"
		responses.Status = 200
		responses.Data = Materials
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Get Sales Order"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}

func AddSalesInvoice(c *gin.Context) {
	// id := c.Param("id")
	sales_id := c.PostForm("sales_id")
	// _, employee_id, _, _ := JWTService.ValidateTokenFromCookies(c)
	// temp_employee_id := strconv.Itoa(employee_id)
	employee_id := c.PostForm("employee_id")

	success := Repo.RepoAddSalesInvoice(sales_id, employee_id, time.Now())

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Get Sales Invoice"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Get Sales Invoice"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}

func GetSalesInvoice(c *gin.Context) {
	id := c.Param("id")

	SalesInvoices := Repo.RepoGetSalesInvoice(id)

	var responses response.GeneralResponse
	if len(SalesInvoices) > 0 {
		responses.Message = "Success Get Sales Invoice"
		responses.Status = 200
		responses.Data = SalesInvoices
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Get Sales Invoice"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}
