package service

import (
	"net/http"
	"strconv"

	Model "dbi-api/modules/sales/data"
	Repo "dbi-api/modules/sales/repository"

	response "dbi-api/responses"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func AddCustomer(c *gin.Context) {
	var Customer Model.External
	Customer.Address = c.PostForm("address")
	Customer.City = c.PostForm("city")
	Customer.Region = c.PostForm("region")
	Customer.Country = c.PostForm("country")
	Customer.Name = c.PostForm("name")
	Customer.PostalCode = c.PostForm("postal_code")

	success := Repo.AddCustomer(Customer)

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Add Customer"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Add Customer"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}

func GetCustomer(c *gin.Context) {

	ID := c.Param("customer_id")

	Materials := Repo.GetCustomer(ID)

	var responses response.GeneralResponse
	if len(Materials) > 0 {
		responses.Message = "Success Get Customer"
		responses.Status = 200
		responses.Data = Materials
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Get Customer"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}

}

func UpdateCustomer(c *gin.Context) {

	var Customer Model.External
	id := c.PostForm("customer_id")
	Customer.ID, _ = strconv.Atoi(c.PostForm("id"))
	Customer = Repo.GetCustomer(id)[0]

	address := c.PostForm("address")
	city := c.PostForm("city")
	country := c.PostForm("country")
	name := c.PostForm("name")
	postal_code := c.PostForm("postal_code")
	region := c.PostForm("region")

	if address != "" {
		Customer.Address = address
	}
	if city != "" {
		Customer.City = city
	}
	if country != "" {
		Customer.Country = country
	}
	if name != "" {
		Customer.Name = name
	}
	if postal_code != "" {
		Customer.PostalCode = postal_code
	}
	if region != "" {
		Customer.Region = region
	}

	success := Repo.UpdateCustomer(Customer)
	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Update Customer"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Update Customer"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}
