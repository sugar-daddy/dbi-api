package repository

import (
	"dbi-api/database"
	Material "dbi-api/modules/inventory/data"
	BOM "dbi-api/modules/production/data"
	"fmt"
	"log"
	"time"
)

func GetBOM(id int) []BOM.BOMDetail {
	db := database.Connect()
	defer db.Close()

	var bomDetails []BOM.BOMDetail
	var bomDetail BOM.BOMDetail

	material_finished := GetUniqueMaterialFinishedOnBOM(id)

	for i := 0; i < len(material_finished); i++ {
		bomDetail.ID = material_finished[i]
		bomDetail.MaterialFinished = GetMaterial(material_finished[i])
		bomDetail.MaterialUsed, bomDetail.Quantity = GetMaterialUsed(material_finished[i])
		bomDetails = append(bomDetails, bomDetail)
	}

	return bomDetails
}

func GetUniqueMaterialFinishedOnBOM(id int) []int {
	db := database.Connect()
	defer db.Close()

	rows, _ := db.Query("SELECT DISTINCT b.material_finished FROM bom b WHERE $1 = 0 OR b.material_finished = $2", id, id)

	var result []int
	var resultScan int

	for rows.Next() {
		if err := rows.Scan(&resultScan); err != nil {
			log.Fatal(err.Error())
		} else {
			result = append(result, resultScan)
		}
	}
	return result
}

func EditBOM(bom BOM.BOMDetail) {
	db := database.Connect()
	defer db.Close()

	count := len(bom.MaterialUsed)

	for i := 0; i < count; i++ {
		db.Exec("INSERT INTO bom(material_finished, material_used, quantity) VALUES ($1, $2, $3)",
			bom.MaterialFinished.ID,
			bom.MaterialUsed[i].ID,
			bom.Quantity[i],
		)
	}
}

func DeleteBOM(id int) bool {
	db := database.Connect()
	defer db.Close()

	_, err := db.Exec("DELETE FROM bom WHERE bom.material_finished = $1", id)

	return err == nil
}

func CheckMaterial(id, quantity []int) bool {
	db := database.Connect()
	defer db.Close()

	var result int
	var resultScan int

	for i := 0; i < len(id); i++ {
		rows, _ := db.Query("SELECT m.quantity FROM material m WHERE m.id = $1", id[i])

		for rows.Next() {
			if err := rows.Scan(&resultScan); err != nil {
				log.Fatal(err.Error())
			} else {
				result = resultScan
			}
		}

		if result < quantity[i] {
			return false
		}
	}

	return true
}

func GetMaterialIdAndQuantityFromRequestId(rId int) (int, int) {
	db := database.Connect()
	defer db.Close()

	var id int
	var qua int
	var idScan int
	var quaScan int

	rows, _ := db.Query("SELECT pg.quantity, pg.material_id FROM request_material rm JOIN produce_goods pg ON rm.production_id = pg.id WHERE pg.id = $1", rId)

	for rows.Next() {
		if err := rows.Scan(&quaScan, &idScan); err != nil {
			log.Fatal(err.Error())
		} else {
			id = idScan
			qua = quaScan
		}
	}

	return id, qua
}

func Request(mat, qua, person int) {
	db := database.Connect()
	defer db.Close()

	fmt.Println("Masuk ke produce goods di request")
	time := time.Now()

	// Insert
	db.Exec("INSERT INTO produce_goods(date, quantity, material_id, employee_id) VALUES ($1, $2, $3, $4)",
		time,
		qua,
		mat,
		person,
	)

	// Get the id that have just been inserted
	rows, _ := db.Query("SELECT pg.id FROM produce_goods pg WHERE pg.date = $1", time)

	var result int
	var resultScan int

	for rows.Next() {
		if err := rows.Scan(&resultScan); err != nil {
			log.Fatal(err.Error())
		} else {
			result = resultScan
		}
	}

	// Insert
	db.Exec("INSERT INTO request_material(date, is_accepted, type, employee_id, production_id) VALUES ($1, $2, $3)",
		time,
		0,
		"production",
		person,
		result,
	)
}

func AddProduceGoods(prod_date time.Time, quantity int, material_id int, employee_id string) bool {
	db := database.Connect()
	defer db.Close()

	fmt.Println("Masuk ke produce goods di add production")
	date := time.Now()
	_, errQuery := db.Exec("INSERT INTO produce_goods(date, produce_date, quantity, material_id, employee_id) VALUES($1,$2,$3,$4,$5)",
		date,
		prod_date,
		quantity,
		material_id,
		employee_id,
	)
	fmt.Println("Line 23 - SalesRepo")
	if errQuery == nil {
		return true
	} else {
		fmt.Println(errQuery)
		return false
	}
}

func GetLastProduceGoodsID() int {
	db := database.Connect()
	defer db.Close()

	var pg_id int
	query := "SELECT MAX(id) FROM produce_goods;"
	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}
	for rows.Next() {
		if err := rows.Scan(&pg_id); err != nil {
			log.Fatal(err.Error())
		}
	}
	fmt.Println("PG ID:", pg_id)

	return pg_id
}

func Convert(mat, qua, rId int, id, quantity []int, production_date string) {
	db := database.Connect()
	defer db.Close()

	// Update request material and produce goods
	db.Exec("UPDATE request_material SET is_accepted = 1 WHERE id = $1", rId)

	// Get the pg id
	rows, _ := db.Query("SELECT pg.id FROM produce_goods pg JOIN request_material rm ON rm.production_id = pg.id WHERE rm.id = $1", rId)

	var pgId int
	var resultScan int

	for rows.Next() {
		if err := rows.Scan(&resultScan); err != nil {
			log.Fatal(err.Error())
		} else {
			pgId = resultScan
		}
	}

	db.Exec("UPDATE produce_goods SET produce_date = $1 WHERE id = $1", pgId)

	// Reduce the quantity
	for i := 0; i < len(id); i++ {
		db.Exec("UPDATE material SET quantity=(SELECT m.quantity FROM material m WHERE m.id = $1) - $2 WHERE id = $3", id[i], quantity[i], id[i])
	}

	// Add the quantity
	db.Exec("UPDATE material SET quantity=(SELECT m.quantity FROM material m WHERE m.id = $1) + $2 WHERE id = $3", mat, qua, mat)
}

func GetMaterial(material_id int) Material.Material {
	db := database.Connect()
	defer db.Close()

	rows, _ := db.Query("SELECT * FROM material m WHERE m.id = $1", material_id)

	var material Material.Material
	var materialScan Material.Material

	for rows.Next() {
		if err := rows.Scan(&materialScan.ID, &materialScan.Measurement, &materialScan.Name, &materialScan.Price, &materialScan.Quantity, &materialScan.Type); err != nil {
			log.Fatal(err.Error())
		} else {
			material = materialScan
		}
	}

	return material
}

func GetMaterialUsed(material_id int) ([]Material.Material, []int) {
	db := database.Connect()
	defer db.Close()

	rows, _ := db.Query("SELECT b.material_used, b.quantity FROM bom b WHERE b.material_finished = $1", material_id)

	var boms []BOM.BOM
	var bomScan BOM.BOM

	for rows.Next() {
		if err := rows.Scan(&bomScan.MaterialUsed, &bomScan.Quantity); err != nil {
			log.Fatal(err.Error())
		} else {
			boms = append(boms, bomScan)
		}
	}

	var mat []Material.Material
	var qua []int

	for i := 0; i < len(boms); i++ {
		mat = append(mat, GetMaterial(boms[i].MaterialUsed))
		qua = append(qua, boms[i].Quantity)
	}

	return mat, qua
}
