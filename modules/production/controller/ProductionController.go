package controller

import (
	JWTService "dbi-api/modules/authentication/service"

	"github.com/gin-gonic/gin"

	ProductionService "dbi-api/modules/production/service"
)

func Routes(router *gin.Engine) {
	production := router.Group("/")
	production.Use(JWTService.Authenticate([]string{"admin", "production"}))
	{
		production.GET("/production", ProductionService.GetBOM)
		production.POST("/production", ProductionService.InsertBOM)
		production.PUT("/production", ProductionService.EditBOM)
		production.DELETE("/production", ProductionService.DeleteBOM)
		production.POST("/production/request", ProductionService.RequestProduction)
	}
}
