package service

import (
	Inventory "dbi-api/modules/inventory/data"
	InventoryRepository "dbi-api/modules/inventory/repository"
	BOM "dbi-api/modules/production/data"
	ProductionRepository "dbi-api/modules/production/repository"
	Response "dbi-api/responses"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

func GetBOM(c *gin.Context) {
	queryId, isFiltered := c.GetQuery("id")

	var id int

	if !isFiltered {
		id = 0
	} else {
		id, _ = strconv.Atoi(queryId)
	}

	bom := ProductionRepository.GetBOM(id)

	var response Response.GeneralResponse

	if len(bom) != 0 {
		response.Message = "OK"
		response.Status = 200
		response.Data = bom
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Error"
		response.Status = 500
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, response)
	}
}

func InsertBOM(c *gin.Context) {
	queryId, isFilledA := c.GetPostForm("id")
	using, isFilledB := c.GetPostForm("using")
	quantity, isFilledC := c.GetPostForm("quantity")

	var id int

	if !isFilledA {
		log.Fatal("Finished material id didn't entered")
	} else {
		id, _ = strconv.Atoi(queryId)
	}

	if !isFilledB {
		log.Fatal("Used material id didn't entered")
	}

	if !isFilledC {
		log.Fatal("Quantity didn't entered")
	}

	_using := strings.Split(using, ",")
	_quantity := strings.Split(quantity, ",")

	if len(_using) != len(_quantity) {
		log.Fatal("Quantity didn't match used material")
	}

	var bomInsert BOM.BOMDetail

	bomInsert.MaterialFinished.ID = id

	for i := 0; i < len(_using); i++ {
		var material_used Inventory.Material
		var quantity int

		material_used.ID, _ = strconv.Atoi(_using[i])
		quantity, _ = strconv.Atoi(_quantity[i])

		bomInsert.MaterialUsed = append(bomInsert.MaterialUsed, material_used)
		bomInsert.Quantity = append(bomInsert.Quantity, quantity)
	}

	ProductionRepository.EditBOM(bomInsert)

	bom := ProductionRepository.GetBOM(id)

	var response Response.GeneralResponse

	if len(bom) != 0 {
		response.Message = "OK"
		response.Status = 200
		response.Data = bom
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Error"
		response.Status = 500
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, response)
	}
}

func EditBOM(c *gin.Context) {
	queryId, isFilledA := c.GetPostForm("id")
	using, isFilledB := c.GetPostForm("using")
	quantity, isFilledC := c.GetPostForm("quantity")

	var id int

	if !isFilledA {
		log.Fatal("Finished material id didn't entered")
	} else {
		id, _ = strconv.Atoi(queryId)
	}

	if !isFilledB {
		log.Fatal("Used material id didn't entered")
	}

	if !isFilledC {
		log.Fatal("Quantity didn't entered")
	}

	_using := strings.Split(using, ",")
	_quantity := strings.Split(quantity, ",")

	if len(_using) != len(_quantity) {
		log.Fatal("Quantity didn't match used material")
	}

	// delete all with finished material id is the input
	ProductionRepository.DeleteBOM(id)

	// insert all with finished material id, and input parameters
	var bomEdit BOM.BOMDetail

	bomEdit.MaterialFinished.ID = id

	for i := 0; i < len(_using); i++ {
		var material_used Inventory.Material
		var quantity int

		material_used.ID, _ = strconv.Atoi(_using[i])
		quantity, _ = strconv.Atoi(_quantity[i])

		bomEdit.MaterialUsed = append(bomEdit.MaterialUsed, material_used)
		bomEdit.Quantity = append(bomEdit.Quantity, quantity)
	}

	ProductionRepository.EditBOM(bomEdit)

	bom := ProductionRepository.GetBOM(id)

	var response Response.GeneralResponse

	if len(bom) != 0 {
		response.Message = "OK"
		response.Status = 200
		response.Data = bom
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Error"
		response.Status = 500
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusInternalServerError, response)
	}
}

func DeleteBOM(c *gin.Context) {
	queryId, isFilled := c.GetQuery("id")

	var id int

	if !isFilled {
		log.Fatal("Finished material id didn't entered")
	} else {
		id, _ = strconv.Atoi(queryId)
	}

	result := ProductionRepository.DeleteBOM(id)

	var response Response.GeneralResponse

	if result {
		response.Message = "OK"
		response.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Error"
		response.Status = 500
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusInternalServerError, response)
	}
}

func RequestProduction(c *gin.Context) {
	formId, isFilledA := c.GetPostForm("id")
	qua, isFilledB := c.GetPostForm("quantity")
	prodDate, isFilledC := c.GetPostForm("produce_date")
	employee_id := c.PostForm("employee_id")

	var id int

	if !isFilledA {
		log.Fatal("Finished material id didn't entered")
	} else {
		id, _ = strconv.Atoi(formId)
	}

	var quantity int
	if !isFilledB {
		log.Fatal("Quantity id didn't entered")
	} else {
		quantity, _ = strconv.Atoi(qua)
	}
	prod_date, _ := time.Parse("2006-01-02", prodDate)
	fmt.Println(prod_date)

	success := ProductionRepository.AddProduceGoods(prod_date, quantity, id, employee_id)

	pg_id := ProductionRepository.GetLastProduceGoodsID()

	if !isFilledC {
		log.Fatal("Produce date didn't entered")
	}

	// get all bom with the id
	boms := ProductionRepository.GetBOM(id)

	var material_used []int
	var material_used_quantity []int
	result := false

	for _, bom := range boms {
		for i := 0; i < len(bom.MaterialUsed); i++ {
			material_used = append(material_used, bom.MaterialUsed[i].ID)
			material_used_quantity = append(material_used_quantity, bom.Quantity[i]*quantity)
		}
	}

	// get materials and  id = material_used have the required material_used_quantity
	// result := ProductionRepository.CheckMaterial(material_used, material_used_quantity)

	if success == true {
		// if material enough add to produce goods and request material
		var request_material Inventory.RequestMaterials
		request_material.Date, _ = time.Parse("2006-01-02", prodDate)
		request_material.Type = "production"
		// result = InventoryRepository.AddRequestMaterial(request_material, "")
		temp_employee_id, _ := strconv.Atoi(employee_id)
		result = InventoryRepository.AddRequestMaterialForProduction(request_material, temp_employee_id, pg_id)
		for i := 0; i < len(material_used); i++ {
			var detail_request_material Inventory.DetailRequestMaterial
			detail_request_material.Amount = material_used_quantity[i]
			detail_request_material.Material.ID = material_used[i]
			result = InventoryRepository.AddDetailRequestMaterial(detail_request_material, -1)
		}
	}

	var response Response.GeneralResponse

	if result && success == true {
		response.Message = "OK"
		response.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Error"
		response.Status = 500
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusInternalServerError, response)
	}
}

func Convert(c *gin.Context) {
	formId, isFilledA := c.GetPostForm("request-id")
	production_date, isFilledB := c.GetPostForm("production-date")

	var rId int

	if !isFilledA {
		log.Fatal("Finished material id didn't entered")
	} else {
		rId, _ = strconv.Atoi(formId)
	}

	if !isFilledB {
		log.Fatal("Production date didn't entered")
	}

	// get material id and quantity from request material

	id, qua := ProductionRepository.GetMaterialIdAndQuantityFromRequestId(rId)

	// get all bom with the id
	bom := ProductionRepository.GetBOM(id)

	var material_used []int
	var material_used_quantity []int

	for i, bill := range bom {
		material_used = append(material_used, bill.MaterialUsed[i].ID)
		material_used_quantity = append(material_used, bill.Quantity[i])
	}

	// get materials and  id = material_used have the required material_used_quantity
	result := ProductionRepository.CheckMaterial(material_used, material_used_quantity)

	// if material enough remove one each
	if result {
		ProductionRepository.Convert(id, qua, rId, material_used, material_used_quantity, production_date)
	}

	var response Response.GeneralResponse

	if result {
		response.Message = "OK"
		response.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Error"
		response.Status = 500
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusInternalServerError, response)
	}
}
