package data

import (
	Material "dbi-api/modules/inventory/data"
)

type BOM struct {
	ID               int `form:"id" json:"id"`
	MaterialFinished int `form:"material_finished" json:"material_finished"`
	MaterialUsed     int `form:"material_used" json:"material_used"`
	Quantity         int `form:"quantity" json:"quantity"`
}

type BOMDetail struct {
	ID               int                 `form:"id" json:"id"`
	MaterialFinished Material.Material   `form:"material_finished" json:"material_finished"`
	MaterialUsed     []Material.Material `form:"material_used" json:"material_used"`
	Quantity         []int               `form:"quantity" json:"quantity"`
}
