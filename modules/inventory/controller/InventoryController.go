package controller

import (
	JWTService "dbi-api/modules/authentication/service"
	"dbi-api/modules/inventory/service"

	"github.com/gin-gonic/gin"
)

func Routes(router *gin.Engine) {
	router.GET("/material", service.GetMaterial)

	inventory := router.Group("/")
	inventory.Use(JWTService.Authenticate([]string{"admin", "inventory"}))
	{
		inventory.POST("/material", service.AddMaterial)
		inventory.PUT("/material", service.UpdateMaterial)

		inventory.POST("/reqmaterial", service.AddRequestMaterial)
		inventory.GET("/reqmaterial", service.GetRequestMaterial)
		inventory.PUT("/reqmaterial/:req_material_id", service.UpdateRequestMaterial)
	}
}
