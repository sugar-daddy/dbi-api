package repository

import (
	"dbi-api/database"
	Model "dbi-api/modules/inventory/data"
	"fmt"
	"log"
	"strconv"
)

func AddRequestMaterial(RequestMaterial Model.RequestMaterials, employee_id string) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("insert into request_material(date, is_accepted, type, employee_id) values($1, $2, $3, $4);",
		RequestMaterial.Date,
		0,
		RequestMaterial.Type,
		nil,
	)

	if errQuery == nil {
		return true
	} else {
		return false
	}
	// return true
}

func AddRequestMaterialForSales(RequestMaterial Model.RequestMaterials, employee_id int, sales_id string) bool {
	db := database.Connect()
	defer db.Close()
	temp, _ := strconv.Atoi(sales_id)

	sqlStatement := `INSERT INTO request_material(date, is_accepted, type, employee_id, production_id, sales_id) values($1, $2, $3, $4, $5, $6)`
	_, err := db.Exec(sqlStatement, RequestMaterial.Date, 0, RequestMaterial.Type, nil, nil, temp)
	if err != nil {
		panic(err)
	}

	// _, errQuery := db.Exec("insert into request_material(date, is_accepted, type, employee_id, purchase_id, production_id, sales_id) values($1, $2, $3, $4, $5, $6, $7);",
	// 	RequestMaterial.Date,
	// 	0,
	// 	RequestMaterial.Type,
	// 	employee_id,
	// 	0,
	// 	0,
	// 	temp,
	// )

	if err == nil {
		return true
	} else {
		return false
	}
	// return true
}

func AddRequestMaterialForProduction(RequestMaterial Model.RequestMaterials, employee_id int, prod_id int) bool {
	db := database.Connect()
	defer db.Close()

	sqlStatement := `INSERT INTO request_material(date, is_accepted, type, employee_id, production_id, sales_id) values($1, $2, $3, $4, $5, $6)`
	_, err := db.Exec(sqlStatement, RequestMaterial.Date, 0, RequestMaterial.Type, nil, prod_id, nil)
	if err != nil {
		panic(err)
	}

	if err == nil {
		return true
	} else {
		return false
	}
	// return true
}

func GetQuantityProduction(prod_id int) (string, string) {
	db := database.Connect()
	defer db.Close()

	var quantity string
	var material_id string
	query := "SELECT quantity, material_id FROM produce_goods WHERE id = " + strconv.Itoa(prod_id)
	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}
	for rows.Next() {
		if err := rows.Scan(&quantity, &material_id); err != nil {
			log.Fatal(err.Error())
		}
	}

	return quantity, material_id
}

func UpdateQuantityMaterial(prod_id *int) bool {
	db := database.Connect()
	defer db.Close()
	quantity, material_id := GetQuantityProduction(*prod_id)
	fmt.Println(quantity, material_id)
	query := "UPDATE material SET quantity = quantity + " + quantity + " WHERE id = " + material_id

	_, errQuery := db.Exec(query)
	if errQuery != nil {
		fmt.Println(errQuery)
		return false
	} else {
		return true
	}

}

func GetRequestMaterial(reqmaterial_id string, employee_id string) []Model.RequestMaterials {
	db := database.Connect()
	defer db.Close()
	query := "SELECT * FROM request_material"

	if reqmaterial_id != "" {
		query += " where id = " + reqmaterial_id
	}
	if employee_id != "" {
		query += " where employee_id = " + employee_id
	}

	query += " ORDER BY id DESC;"

	rows, err := db.Query(query)
	if err != nil {
		fmt.Println(err)
	}

	var RequestMaterial Model.RequestMaterials
	var listRequestMaterial []Model.RequestMaterials
	var temp *string

	for rows.Next() {
		if err := rows.Scan(&RequestMaterial.ID, &RequestMaterial.Date, &RequestMaterial.IsAccepted, &RequestMaterial.Type, &temp, &RequestMaterial.ProductionID, &RequestMaterial.SalesID); err != nil {
			log.Fatal(err.Error())
		} else {
			listRequestMaterial = append(listRequestMaterial, RequestMaterial)
		}
	}

	for i := 0; i < len(listRequestMaterial); i++ {
		listRequestMaterial[i].ListDetailRequest = GetDetailRequestMaterial("", strconv.Itoa(listRequestMaterial[i].ID))

	}

	return listRequestMaterial
}

func UpdateRequestMaterial(reqmaterial_id string, isStatus string, employee_id string) bool {
	db := database.Connect()
	defer db.Close()
	query := "UPDATE request_material"

	if isStatus != "" {
		query += " SET is_accepted = " + isStatus
	}

	query += " WHERE id = " + reqmaterial_id + ";"
	_, errQuery := db.Exec(query)

	query = "UPDATE request_material"

	if employee_id != "" {
		query += " SET employee_id = " + employee_id
	}

	query += " WHERE id = " + reqmaterial_id + ";"

	_, errQuery2 := db.Exec(query)
	fmt.Println(errQuery2)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}

func UpdateStatusRequestMaterial(reqMateiral Model.RequestMaterials) bool {
	db := database.Connect()
	defer db.Close()
	// query := "UPDATE request_material"

	listDetailMaterial := reqMateiral.ListDetailRequest

	var quantity int

	success := true

	var listQuantity []int

	for i := 0; i < len(listDetailMaterial); i++ {
		query := "SELECT quantity FROM material where id = " + strconv.Itoa(listDetailMaterial[i].Material.ID) + ";"
		rows, err := db.Query(query)
		if err != nil {
			log.Println(err)
		}
		for rows.Next() {
			if err := rows.Scan(&quantity); err != nil {
				log.Fatal(err.Error())
			}
		}

		if quantity < listDetailMaterial[i].Amount {
			success = false
		} else {
			quantity = quantity - listDetailMaterial[i].Amount
			listQuantity = append(listQuantity, quantity)
		}
	}
	successupdate := true

	if success == false {
		return false
	} else {
		for i := 0; i < len(listDetailMaterial); i++ {
			query := "UPDATE material SET quantity = " + strconv.Itoa(listQuantity[i]) + " Where id = " + strconv.Itoa(listDetailMaterial[i].Material.ID)
			_, errQuery := db.Exec(query)
			if errQuery != nil {
				successupdate = false
			}
		}
	}
	if successupdate == true {
		return true
	} else {
		return false
	}
}
