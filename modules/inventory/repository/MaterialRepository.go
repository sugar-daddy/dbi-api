package repository

import (
	"dbi-api/database"
	Model "dbi-api/modules/inventory/data"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

func AddMaterial(Material Model.Material) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("INSERT INTO material(measurement, name, price, quantity, type) VALUES($1,$2,$3,$4,$5)",
		Material.Measurement,
		Material.Name,
		Material.Price,
		Material.Quantity,
		Material.Type,
	)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}

func GetMaterial(id string, tipe string, notSame string) []Model.Material {
	db := database.Connect()
	defer db.Close()
	query := "SELECT * FROM material"

	if id != "" {
		query += " where id = " + id + ""
	}
	if tipe != "" {
		query += " where type = '" + tipe + "'"
	}
	if notSame != "" {
		query += " where type != '" + notSame + "'"
	}

	query += " ORDER BY id DESC;"

	rows, err := db.Query(query)
	if err != nil {
		fmt.Println(err)
	}

	var Material Model.Material
	var Materials []Model.Material

	for rows.Next() {
		if err := rows.Scan(&Material.ID, &Material.Measurement, &Material.Name,
			&Material.Price, &Material.Quantity, &Material.Type); err != nil {
			log.Fatal(err.Error())
		} else {
			Materials = append(Materials, Material)
		}
	}

	return Materials
}

func UpdateMaterial(Material Model.Material) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("UPDATE material SET measurement = $1, name = $2, price = $3, quantity = $4, type = $5 WHERE id = $6;",
		Material.Measurement,
		Material.Name,
		Material.Price,
		Material.Quantity,
		Material.Type,
		Material.ID,
	)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}

func DeleteMaterial(id string) bool {
	db := database.Connect()
	defer db.Close()
	_, errQuery := db.Exec("DELETE FROM material WHERE id = " + id)
	if errQuery == nil {
		return true
	} else {
		return false
	}
}
