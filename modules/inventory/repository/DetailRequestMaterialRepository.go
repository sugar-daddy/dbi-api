package repository

import (
	"dbi-api/database"
	Model "dbi-api/modules/inventory/data"
	"fmt"
	"log"
	"strconv"
	"strings"
)

func AddDetailRequestMaterial(DetailRequestMaterial Model.DetailRequestMaterial, req_material_id int) bool {
	db := database.Connect()
	defer db.Close()

	if req_material_id == -1 {
		req_material_id = getLastRequestMaterialId()
	}

	_, errQuery := db.Exec("INSERT INTO detail_request_material(amount, material_id, request_material_id) VALUES($1,$2,$3)",
		DetailRequestMaterial.Amount,
		DetailRequestMaterial.Material.ID,
		req_material_id,
	)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}

func InsertDetailRequestMaterial(amount, material_id string) bool {
	db := database.Connect()
	defer db.Close()

	reqMatId := getLastRequestMaterialId()

	listAmount := strings.Split(amount, ",")
	listMaterialId := strings.Split(material_id, ",")
	success := true

	for i := 0; i < len(listAmount); i++ {
		_, errQuery := db.Exec("INSERT INTO detail_request_material(amount, material_id, request_material_id) VALUES($1,$2,$3)",
			listAmount[i],
			listMaterialId[i],
			reqMatId,
		)

		if errQuery != nil {
			success = false
		}

	}

	if success == true {
		return true
	} else {
		return false
	}

}

func getLastRequestMaterialId() int {
	db := database.Connect()
	defer db.Close()

	var req_material_id int
	query := "SELECT MAX(id) FROM request_material;"
	rows, err := db.Query(query)
	if err != nil {
		log.Println(err)
	}
	for rows.Next() {
		if err := rows.Scan(&req_material_id); err != nil {
			log.Fatal(err.Error())
		}
	}

	return req_material_id
}

func GetDetailRequestMaterial(detreqmaterial_id string, reqmat_id string) []Model.DetailRequestMaterial {
	db := database.Connect()
	defer db.Close()
	query := "SELECT * FROM detail_request_material"

	if detreqmaterial_id != "" {
		query += " where id = " + detreqmaterial_id
	}
	if reqmat_id != "" {
		query += " where request_material_id = " + reqmat_id
	}

	query += " ORDER BY id DESC;"

	rows, err := db.Query(query)
	if err != nil {
		fmt.Println(err)
	}

	var DetailRequestMaterial Model.DetailRequestMaterial
	var listDetailRequestMaterial []Model.DetailRequestMaterial
	var temp string

	for rows.Next() {
		if err := rows.Scan(&DetailRequestMaterial.ID, &DetailRequestMaterial.Amount, &DetailRequestMaterial.Material.ID, &temp); err != nil {
			log.Fatal(err.Error())
		} else {
			listDetailRequestMaterial = append(listDetailRequestMaterial, DetailRequestMaterial)
		}
	}

	for i := 0; i < len(listDetailRequestMaterial); i++ {
		listDetailRequestMaterial[i].Material = GetMaterial(strconv.Itoa(listDetailRequestMaterial[i].Material.ID), "", "")[0]
	}

	return listDetailRequestMaterial
}

func DeleteDetailRequestMaterial(detail_req_material_id string) bool {
	db := database.Connect()
	defer db.Close()

	query := "DELETE FROM detail_request_material WHERE id = " + detail_req_material_id + ";"
	_, errQuery := db.Exec(query)

	if errQuery == nil {
		return true
	} else {
		return false
	}
}
