package data

import (
	"time"
)

type RequestMaterials struct {
	ID                int                     `form:"id" json:"id"`
	Date              time.Time               `form:"date" json:"date"`
	IsAccepted        int                     `form:"isaccepted" json:"isaccepted"`
	ListDetailRequest []DetailRequestMaterial `form:"listdetailrequest" json:"listdetailrequest"`
	Type              string                  `form:"type" json:"type"`
	ProductionID      *int                    `form:"production_id" json:"production_id"`
	SalesID           *int                    `form:"sales_id" json:"sales_id"`
}
