package data

type DetailRequestMaterial struct {
	ID       int      `form:"id" json:"id"`
	Material Material `form:"material" json:"material"`
	Amount   int      `form:"amount" json:"amount"`
}
