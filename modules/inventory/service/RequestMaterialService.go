package service

import (
	// JWTService "dbi-api/modules/authentication/service"
	Model "dbi-api/modules/inventory/data"
	Repo "dbi-api/modules/inventory/repository"

	// Repo2 "dbi-api/modules/sales/repository"
	response "dbi-api/responses"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func AddRequestMaterial(c *gin.Context) {
	var RequestMaterial Model.RequestMaterials
	date := c.PostForm("date")
	if date == "" {
		t := time.Now()
		RequestMaterial.Date = t

	} else {
		t, _ := time.Parse(date, "2016-06-10")
		RequestMaterial.Date = t
	}

	employee_id := c.PostForm("employee_id")
	RequestMaterial.Type = c.PostForm("type")

	amount := c.PostForm("amount")
	material_id := c.PostForm("material_id")

	// list_amount := strings.Split(amount, "-")
	// list_material_id := strings.Split(material_id, "-")

	success := Repo.AddRequestMaterial(RequestMaterial, employee_id)
	success = Repo.InsertDetailRequestMaterial(amount, material_id)

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Add Request Material"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Add Request Material"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}

func AddRequestMaterialForSales(empolyee_id int, amount string, material_id string, sales_id string) bool {
	var RequestMaterial Model.RequestMaterials

	t := time.Now()
	RequestMaterial.Date = t
	RequestMaterial.Type = "sales"

	// list_amount := strings.Split(amount, "-")
	// list_material_id := strings.Split(material_id, "-")

	success := Repo.AddRequestMaterialForSales(RequestMaterial, empolyee_id, sales_id)
	success = Repo.InsertDetailRequestMaterial(amount, material_id)

	return success
}

func GetRequestMaterial(c *gin.Context) {

	req_material_id := c.Param("req_material_id")
	employee_id := c.PostForm("employee_id")

	DetailRequestMaterials := Repo.GetRequestMaterial(req_material_id, employee_id)

	var responses response.GeneralResponse
	if len(DetailRequestMaterials) > 0 {
		responses.Message = "Success Get All Detail Request Material"
		responses.Status = 200
		responses.Data = DetailRequestMaterials
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Get Detail Request Material"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}

func UpdateRequestMaterial(c *gin.Context) {
	reqmaterial_id := c.Param("req_material_id")
	status := c.PostForm("status")
	employee_id := c.PostForm("employee_id")

	success := false

	if status == "1" {
		reqmaterial := Repo.GetRequestMaterial(reqmaterial_id, "")[0]

		isSuccess := Repo.UpdateStatusRequestMaterial(reqmaterial)
		if isSuccess != false {
			if reqmaterial.ProductionID != nil {
				success = Repo.UpdateQuantityMaterial(reqmaterial.ProductionID)
			}
			success = Repo.UpdateRequestMaterial(reqmaterial_id, status, employee_id)
		}
	} else if status == "2" {
		success = Repo.UpdateRequestMaterial(reqmaterial_id, status, employee_id)
	}

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Update Request Material"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Update Request Material"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}
