package service

import (
	"net/http"
	"strconv"

	Model "dbi-api/modules/inventory/data"
	Repo "dbi-api/modules/inventory/repository"

	response "dbi-api/responses"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func AddMaterial(c *gin.Context) {
	measurement := c.PostForm("measurement")
	name := c.PostForm("name")
	price, _ := strconv.Atoi(c.PostForm("price"))
	quantity, _ := strconv.Atoi(c.PostForm("quantity"))
	tipe := c.PostForm("type")
	var Material Model.Material
	Material.Measurement = measurement
	Material.Name = name
	Material.Price = price
	Material.Quantity = quantity
	Material.Type = tipe

	success := Repo.AddMaterial(Material)

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Add Material"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Add Material"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}

func GetMaterial(c *gin.Context) {

	ID := c.Param("material_id")
	tipe := c.Query("type")
	notSame := c.Query("not_same")
	Materials := Repo.GetMaterial(ID, tipe, notSame)

	var responses response.GeneralResponse
	if len(Materials) > 0 {
		responses.Message = "Success Get All Material"
		responses.Status = 200
		responses.Data = Materials
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Get Material"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}

func UpdateMaterial(c *gin.Context) {

	id := c.PostForm("id")
	measurement := c.PostForm("measurement")
	name := c.PostForm("name")
	var price = 0
	price, _ = strconv.Atoi(c.PostForm("price"))
	var quantity = 0
	quantity, _ = strconv.Atoi(c.PostForm("quantity"))
	tipe := c.PostForm("type")
	Material := Repo.GetMaterial(id, "", "")[0]

	if measurement != "" {
		Material.Measurement = measurement
	}
	if name != "" {
		Material.Name = name
	}
	if price != 0 {
		Material.Price = price
	}
	if quantity != 0 {
		Material.Quantity = quantity
	}
	if tipe != "" {
		Material.Type = tipe
	}

	success := Repo.UpdateMaterial(Material)

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Update Material"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Update Material"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}
