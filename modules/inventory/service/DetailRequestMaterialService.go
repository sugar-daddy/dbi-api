package service

import (
	Model "dbi-api/modules/inventory/data"
	Repo "dbi-api/modules/inventory/repository"
	response "dbi-api/responses"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func AddDetailRequestMaterial(c *gin.Context) {
	// date := c.PostForm("date")
	// if date == "" {
	// 	t := time.Now()
	// 	date = t.String()

	// }else{
	// 	t, _ := time.Parse(date, "2016-06-10")
	// 	date = t.String()
	// }
	// isAccepted := c.PostForm("name")
	// employee_id, _ := strconv.Atoi(c.PostForm("price"))
	// quantity, _ := strconv.Atoi(c.PostForm("quantity"))
	// tipe := c.PostForm("type")
	amount, _ := strconv.Atoi(c.PostForm("amount"))
	material_id, _ := strconv.Atoi(c.PostForm("material_id"))
	req_material_id, _ := strconv.Atoi(c.PostForm("req_material_id"))
	var DetailRequestMaterial Model.DetailRequestMaterial
	DetailRequestMaterial.Amount = amount
	DetailRequestMaterial.Material.ID = material_id

	success := Repo.AddDetailRequestMaterial(DetailRequestMaterial, req_material_id)

	var responses response.GeneralResponse
	if success == true {
		responses.Message = "Success Add Detail Request Material"
		responses.Status = 200
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Add Detail Request Material"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}

}

func GetDetailRequestMaterial(c *gin.Context) {

	detrail_req_material_id := c.Param("detrail_req_material_id")
	req_material_id := c.PostForm("req_material_id")

	DetailRequestMaterials := Repo.GetDetailRequestMaterial(detrail_req_material_id, req_material_id)

	var responses response.GeneralResponse
	if len(DetailRequestMaterials) > 0 {
		responses.Message = "Success Get All Detail Request Material"
		responses.Status = 200
		responses.Data = DetailRequestMaterials
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, responses)
	} else {
		responses.Message = "Failed Get Detail Request Material"
		responses.Status = 400
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, responses)
	}
}
