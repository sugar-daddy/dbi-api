package repository

import (
	"dbi-api/database"
	"log"
)

func GetTotalSales() int {
	db := database.Connect()
	defer db.Close()

	var sumRow int
	var sum int

	rows, _ := db.Query("SELECT SUM(sd.price) FROM sales_detail sd")

	for rows.Next() {
		if err := rows.Scan(&sumRow); err != nil {
			log.Fatal(err.Error())
		} else {
			sum = sumRow
		}
	}

	return sum
}

func GetBestSeller() string {
	db := database.Connect()
	defer db.Close()

	var idRow int
	var id int

	var bsRow string
	var bs string

	rows1, _ := db.Query("SELECT sd.material_id FROM sales_detail sd WHERE sd.quantity = (SELECT MAX(quantity) FROM sales_detail)")

	for rows1.Next() {
		if err := rows1.Scan(&idRow); err != nil {
			log.Fatal(err.Error())
		} else {
			id = idRow
		}
	}

	rows2, _ := db.Query("SELECT m.name FROM material m WHERE m.id =$1", id)

	for rows2.Next() {
		if err := rows2.Scan(&bsRow); err != nil {
			log.Fatal(err.Error())
		} else {
			bs = bsRow
		}
	}

	return bs
}

func GetNetSales() float64 {
	return GetGrossSales() - GetSumExpense()
}

func GetGrossSales() float64 {
	db := database.Connect()
	defer db.Close()

	var sumRow float64
	var sum float64

	rows, _ := db.Query("SELECT SUM(cr.amount) FROM cash_report cr WHERE cr.status = 'debit'")

	for rows.Next() {
		if err := rows.Scan(&sumRow); err != nil {
			log.Fatal(err.Error())
		} else {
			sum = sumRow
		}
	}

	return sum
}

func GetTotalExpense() int {
	db := database.Connect()
	defer db.Close()

	var sumRow int
	var sum int

	rows, _ := db.Query("SELECT SUM(pd.price) FROM purchase_detail pd")

	for rows.Next() {
		if err := rows.Scan(&sumRow); err != nil {
			log.Fatal(err.Error())
		} else {
			sum = sumRow
		}
	}

	return sum
}

func GetHighestExpense() string {
	db := database.Connect()
	defer db.Close()

	var idRow int
	var id int

	var heRow string
	var he string

	rows1, _ := db.Query("SELECT pd.material_id FROM purchase_detail pd WHERE pd.quantity = (SELECT MAX(quantity) FROM purchase_detail)")

	for rows1.Next() {
		if err := rows1.Scan(&idRow); err != nil {
			log.Fatal(err.Error())
		} else {
			id = idRow
		}
	}

	rows2, _ := db.Query("SELECT m.name FROM material m WHERE m.id =$1", id)

	for rows2.Next() {
		if err := rows2.Scan(&heRow); err != nil {
			log.Fatal(err.Error())
		} else {
			he = heRow
		}
	}

	return he
}

func GetSumExpense() float64 {
	db := database.Connect()
	defer db.Close()

	var sumRow float64
	var sum float64

	rows, _ := db.Query("SELECT SUM(cr.amount) FROM cash_report cr WHERE cr.status = 'credit'")

	for rows.Next() {
		if err := rows.Scan(&sumRow); err != nil {
			log.Fatal(err.Error())
		} else {
			sum = sumRow
		}
	}

	return sum
}
