package data

type SalesIncomeDTO struct {
	TotalSales int
	BestSeller string
	NetSales   float64
	GrossSales float64
}

type ExpenseDTO struct {
	TotalExpense   int
	HighestExpense string
	SumExpense     float64
}
