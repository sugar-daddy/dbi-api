package controller

import (
	JWTService "dbi-api/modules/authentication/service"

	"github.com/gin-gonic/gin"

	SummaryService "dbi-api/modules/summary/service"
)

func Routes(router *gin.Engine) {
	summary := router.Group("/")
	summary.Use(JWTService.Authenticate([]string{"admin"}))
	{
		summary.GET("/summary/sales-invoice-summary", SummaryService.GetSalesAndIncomeSummary)
		summary.GET("/summary/expense-summary", SummaryService.GetExpenseSummary)
	}
}
