package service

import (
	"net/http"

	"github.com/gin-gonic/gin"

	DTO "dbi-api/modules/summary/data"
	SummaryRepository "dbi-api/modules/summary/repository"
	Response "dbi-api/responses"
)

func GetSalesAndIncomeSummary(c *gin.Context) {
	var dto DTO.SalesIncomeDTO

	dto = getSalesAndIncomeSummary()

	var response Response.GeneralResponse

	if dto != (DTO.SalesIncomeDTO{}) {
		response.Message = "OK"
		response.Status = 200
		response.Data = dto
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Unauthorized"
		response.Status = 401
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, response)
	}
}

func GetExpenseSummary(c *gin.Context) {
	var dto DTO.ExpenseDTO

	dto = getExpenseSummary()

	var response Response.GeneralResponse

	if dto != (DTO.ExpenseDTO{}) {
		response.Message = "OK"
		response.Status = 200
		response.Data = dto
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK, response)
	} else {
		response.Message = "Unauthorized"
		response.Status = 401
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusUnauthorized, response)
	}
}

func getSalesAndIncomeSummary() DTO.SalesIncomeDTO {
	var dto DTO.SalesIncomeDTO

	dto.TotalSales = SummaryRepository.GetTotalSales()
	dto.BestSeller = SummaryRepository.GetBestSeller()
	dto.NetSales = SummaryRepository.GetNetSales()
	dto.GrossSales = SummaryRepository.GetGrossSales()

	return dto
}

func getExpenseSummary() DTO.ExpenseDTO {
	var dto DTO.ExpenseDTO

	dto.TotalExpense = SummaryRepository.GetTotalExpense()
	dto.HighestExpense = SummaryRepository.GetHighestExpense()
	dto.SumExpense = SummaryRepository.GetSumExpense()

	return dto
}
