package app

import (
	"dbi-api/config"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func StartApplication() {
	serverPort := config.LoadConfig("PORT")

	router := gin.Default()
	c := cors.DefaultConfig()
	c.AllowOrigins = []string{
		config.LoadConfig("ORIGIN1"),
		config.LoadConfig("ORIGIN2"),
	}
	c.AllowCredentials = true
	c.AllowMethods = []string{
		"GET",
		"POST",
		"PUT",
		"DELETE",
	}
	c.AllowHeaders = []string{
		"Origin",
		"Content-Type",
		"Accept",
	}
	c.ExposeHeaders = []string{
		"*",
	}
	router.Use(cors.New(c))

	MapEndPoints(router)

	router.Run(":" + serverPort)
}
