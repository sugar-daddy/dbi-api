package app

import (
	authentication "dbi-api/modules/authentication/controller"
	finance "dbi-api/modules/finance/controller"
	inventory "dbi-api/modules/inventory/controller"
	personal "dbi-api/modules/personal/controller"
	procurement "dbi-api/modules/procurement/controller"
	production "dbi-api/modules/production/controller"
	sales "dbi-api/modules/sales/controller"
	summary "dbi-api/modules/summary/controller"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func MapEndPoints(router *gin.Engine) {
	summary.Routes(router)
	finance.Routes(router)
	production.Routes(router)
	inventory.Routes(router)
	personal.Routes(router)
	procurement.Routes(router)
	sales.Routes(router)
	authentication.Routes(router)
}
