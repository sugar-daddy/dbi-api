### API docs

[Trello](https://trello.com/b/8uvmeWfM/dbi-back-end)

## Setup
1. Create a PostgreSQL database named `DonutBusinessInternational`
1. Prepare the tables
1. Make a .env file according to .env.example
1. Run this code to install dependencies
```
go get
```

## Run
```
go run main.go
```
